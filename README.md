# DCDP-Server

This is a cross platform server implementing the Dynamic Control Data Protocol (DCDP, DCD Protocol). Tested platforms are Linux (including Raspberry Pi), Windows (10,11) and MacOS.

The project supports varying network transports through modules, of which UDP, TCP and MQTT are provided in this project. Further modules may be added according to user requirements.

It is able to support varying hardware devices through modules. Two such modules are provided in this project, supporting a range of [X-keys](https://xkeys.com/) and [Streamdeck](https://www.elgato.com/en/stream-deck) devices. Modules for other devices may be added according to user requirements.


### Installation

End user [installation packages which will eventually be available here](https://gitlab.com/chris.willing.oss/dcdp-server/-/releases) are self contained and should have no additional software requirements. Until then, testing and [feedback](https://gitlab.com/chris.willing.oss/dcdp-server/-/issues) is invited using this repository directly. 

###### Prerequisites

The DCD Protocol is itself language agnostic. This implementation is written in NodeJS and any testing, modifications or development will require NodeJS to be installed.

Communication between client applications and the _dcdp-server_ may be via TCP, UDP or MQTT protocol. Client applicatons using UDP are restricted to the local network. MQTT clients will communicate via an MQTT broker e.g. a [_mosquitto_](https://mosqitto.org) instance running on the host machine (although any MQTT broker across the world could be used).
```
    sudo apt install mosquitto
```
A typical MQTT use-case is that of multiple Node-RED nodes enabling interaction with attached X-keys devices. Examples may be found in the [node-red-contrib-dcdp project](https://gitlab.com/chris.willing.oss/node-red-contrib-dcdp).

Service discovery with mDNS/SD is supported but may require additional system support e.g. _Avahi_ installed on Linux systems.

###### Install & run directly from the repository
Before running the DCDP server, please ensure that no instance of _xkeys-server_ (this project's predecessor) is running. If neccessary, it may be stopped and disabled with the following commands (Debian, Ubuntu, Raspberry Pi, etc.)
```
    sudo systemctl stop xkeys-server
    sudo systemctl disable xkeys-server
    sudo apt remove xkeys-server
```
When installing the _dcdp-server_, some libraries may have to be rebuilt for the host machine architecture when the _npm install_ command is run. This requires some development tools and additional libraries to be installed e.g.
```
    sudo apt install -y build-essential nodejs npm git libudev-dev libusb-1.0-0-dev libavahi-compat-libdnssd-dev cmake
```
Now, to install and run the _dcdp-server_ during this testing phase, run the following commands in a terminal:
```
    git clone https://gitlab.com/chris.willing.oss/dcdp-server.git
    cd dcdp-server
    npm install
    node scripts/dcdp-server.js
```
Before running the _dcdp-server_ for the first time on Linux or Raspberry Pi systems, adjustments to access permissions for X-keys devices are needed. Save the following to _/etc/udev/rules.d/50-xkeys.rules_ and reload the rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
```
SUBSYSTEM=="input", GROUP="input", MODE="0666"
SUBSYSTEM=="usb", ATTRS{idVendor}=="05f3", MODE:="0666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="05f3", MODE="0666", GROUP="plugdev"
```
A similar facility is required for Elgato Streamdeck devices. In this case, save the following to /etc/udev/rules.d/50-elgato.rules; then reload the rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
```
SUBSYSTEM=="input", GROUP="input", MODE="0666"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0060", MODE:="666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0063", MODE:="666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006c", MODE:="666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006d", MODE:="666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0080", MODE:="666", GROUP="plugdev"
SUBSYSTEM=="usb", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0086", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0060", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0063", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006c", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="006d", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0080", MODE:="666", GROUP="plugdev"
KERNEL=="hidraw*", ATTRS{idVendor}=="0fd9", ATTRS{idProduct}=="0086", MODE:="666", GROUP="plugdev"
```



### Support
Problems, comments and questions should be directed to the [Issues](https://gitlab.com/chris.willing.oss/dcdp-server/-/issues) section.

### Contributing
Merge Requests to fix bugs are welcome via the [Merge requests](https://gitlab.com/chris.willing.oss/dcdp-server/-/merge_requests[](url)) section. Major feature additions are probably best discussed first in the [Issues](https://gitlab.com/chris.willing.oss/dcdp-server/-/issues) section.

### Authors and acknowledgment
Many thanks to [P.I. Engineering](https://xkeys.com/) for financial support and donation of several hardware devices for development and testing.

Thanks also to the authors and contributers to the [SuperFly TV](https://github.com/SuperFlyTV/xkeys) and [Julusian](https://github.com/Julusian/node-elgato-stream-deck) projects.


### License
MIT


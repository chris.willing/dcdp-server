# Packaging for _apt_ based package managers

This is the directory from which installable _.deb_ packages are created for Linux distros using _apt_ based package managers. To create a package, run the make-deb script here on an approriate machine. On successful completion, the resulting package's location is reported. It can be installed with (insert correct version):
```
	sudo apt install ./dcdp-server-0.2.1_amd64.deb
```

Please note that packages created here are **not** suitable for the Raspberry Pi. Please see the _raspi_ directory instead.

# Packaging for Slackware.

This is the directory from which installable Slackware packages are created.

To create a package, run the make-slack script here on an approriate machine.
This produces a dcdp-server.SlackBuild file which can be run (as root) to produce the dcdp-server package.
Ensure the correct source tarball is available when running the SlackBuild.

On successful completion, the resulting package can be installed with (insert correct location & version):
```
	sudo upgradepkg --install-new --reinstall /tmp/dcdp-server-0.2.2-x86_64-1_dcd.tgz
```

The package provides an rc.dcdp-server file to easily start/stop/restart the server. It may be started
automatically at boot time by addding the following to /etc/rc.d/rc.local:
# Start dcdp-server
if [ -x /etc/rc.d/rc.dcdp-server ]; then
  . /etc/rc.d/rc.dcdp-server start
fi


# Packaging the dcdp-server for Raspberry Pi
The usual way to generate an installable dcdp-server package
for the Raspberry Pi is to run the _make-raspi_ script.

First, install prerequisite development packages with:
```
	sudo apt install build-essential git
```
No third party repos are required provided _apt_ is  configured for both _main_ and _contrib_ branches. Install other packages required for packaging _dcdp-server_ with:
```
	sudo apt install nodejs npm libuv1-dev libudev-dev libfuse-dev libusb-1.0-0-dev libavahi-compat-libdnssd-dev
```

After running _make-raspi_, the location of the resulting .deb package will be reported (probably here). It can be installed with (insert correct version):
```
	sudo apt install ./dcdp-server-0.2.1_armhf.deb
```


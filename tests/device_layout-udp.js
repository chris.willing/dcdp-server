#!/usr/bin/env node


/*	device_layout-udp.js
*
*	SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*	SPDX-FileCopyrightText: 2023 Christoph Willing <chris.willing@linux.com>
* 
*	A client for testing UDP communication with a dcdp-server.
*	Find server (possibly nominated), connect and request device layout data.
*	The layout data is saved in a file (as found by the server).
*/

const fs = require('fs');
const path = require('path');

let target_serverId;
const myArgs = process.argv.slice(2);
if (myArgs.length > 0) {
   target_serverId = myArgs[0];
}

const server_port = 48895;
let server_addr;
const discovered_hosts = [];

const dgram = require('dgram');
const client = dgram.createSocket('udp4');
client.bind( () => {
	client.setBroadcast(true);
});


client.on('message', (message, remote) => {
	//console.log('Msg Rcvd: ' + JSON.stringify(JSON.parse(message), null, 2) + "  from: " + JSON.stringify(remote));
	//console.log(`Msg Rcvd: ${JSON.stringify(JSON.parse(message))} from: ${JSON.stringify(remote)})`);
	var msg = "";
	try{
		msg = JSON.parse(message);
		let msg_type;
		if (msg.hasOwnProperty('msg_type')) {
			msg_type = 'msg_type';
		} else {
			msg_type = 'request';
		}
		if (msg[msg_type] == "discover_result") {
			console.log(`Received a discover_result message. Server address: ${msg.xk_server_address}`);
			if (discovered_hosts.find(entry => { return entry.xk_server_address === msg.xk_server_address ; }) ) {
				/* We have already seen this server */
				console.log(`Not adding duplicate ${msg.xk_server_address}`);
			} else {
				console.log(`Adding server: ${JSON.stringify(msg)}`);
				discovered_hosts.push(msg);
			}
		} else if (msg[msg_type] == "connect_result") {
			console.log(`Received connect_result: ${JSON.stringify(msg)}`);
			begin_normal_operations();
		} else if (msg[msg_type] == "list_attached_result") {
			var device_keys = Object.keys(msg.devices);
			if (device_keys.length > 0 ){
				console.log(JSON.stringify(msg.devices));
				/* Choose a device randomly to display */
				var choice = Math.floor(Math.random() * device_keys.length);
				console.log(`Showing number ${(choice + 1)} of ${device_keys.length} attached device(s) (${device_keys[choice]}):`);
				console.log(JSON.stringify(msg.devices[device_keys[choice]], null, 2));
			} else {
				console.log(`No devices attached at ${msg.server_id}`);
			}
		} else if (msg[msg_type] == "product_list_result") {
			var product_keys = Object.keys(msg.data);
			console.log(`Product List length = ${JSON.stringify(msg).length}`);
			if (product_keys.length > 0 ){
				/* Choose a device randomly to display */
				var choice = Math.floor(Math.random() * product_keys.length);
				console.log(`Showing number ${(choice + 1)} of ${product_keys.length} product(s):`);
				console.log(JSON.stringify(msg.data[product_keys[choice]]));
			} else {
				console.log(`No products available at ${msg.server_id}`);
			}
		} else if (msg[msg_type] == "device_layout_result") {
			//console.log(`device_layout_result: ${JSON.stringify(msg.svg_layout)}`);
			const layout = msg.svg_layout;
			const device_name = Object.keys(layout)[0];
			console.log(`device_name: ${device_name}`);
			Object.keys(layout[device_name]).forEach( (svg_file_name) => {
			    console.log(`file_name: ${svg_file_name}`);
			    try {
				fs.writeFileSync(path.join(__dirname, svg_file_name), Buffer.from(layout[device_name][svg_file_name]));
			    }
			    catch (err) {
				console.error(err);
			    }
			});
			process.exit(0);
		} else if (msg[msg_type] == "command_result") {
			console.log(`Received command_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "list_clients_result") {
			console.log(`Received list_clients_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "disconnect_warning") {
			console.log(`Received disconnect_warning: ${JSON.stringify(msg)}`);
			console.log(`${Date.call()}`);
			setTimeout(send_udp_message, 10, (new Buffer.from('{"msg_type":"connect"}', 'UTF-8')));
		} else if (msg[msg_type] == "disconnect_result") {
			console.log(`Received disconnect_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "error") {
			console.log(`Received ERROR msg: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "button_event") {
			// Reflect test
			console.log(`Received REFLECTED message: ${JSON.stringify(msg)}`);
		} else if (/.*_event/.exec(msg[msg_type])) {
			/*	See all events */
			console.log(JSON.stringify(JSON.parse(message)));

			/* OR, to filter to see only "down" events
			*  - could be "up", "tbar", "jog", "shuttle", ...
			*
				if (msg.data.type == "down") {
					console.log(msg);
				}
			*/
		} else {
		}
	}
	catch (err) {
	}
});

send_udp_message = (message) => {
	/*	Before sending message, check that it's valid JSON */
	/*
	var msg = "";
	try {
		msg = JSON.parse(message);
	}
	catch (err) {
		console.log(`Not sending invalid message: ${message}`);
		console.log(err);
		return;
	}
	*/

	client.send(message, 0, message.length, server_port, server_addr, (err, bytes) => {
		if (err) {
			throw err;
		}
		//console.log(`Sending ${msg["msg_type"]} request to ${server_addr}:${server_port}`);
	});
}

choose_server = (server_id) => {
	if (discovered_hosts.length == 0) {
		/* No servers found so try again
		*/
		console.log("Finding server ...");
		try {
			client.setBroadcast(true);
		}
		catch (err) {
		}
		client.send(discovery_message, 0, discovery_message.length, server_port, '255.255.255.255', function(err, bytes) { });
		setTimeout(choose_server, 1000, server_id);
	} else {
		if (server_id) {
			/* Extract the entry with matching SID
			*/
			const target = discovered_hosts.find(entry => { return entry.server_id === server_id ; });
			if (target) {
				console.log(`Choice: ${JSON.stringify(target)}`);
				console.log(`Choice: ${target.server_id} at ${target.xk_server_address}`);
				server_addr = target.xk_server_address;
				client.setBroadcast(false);
				send_udp_message(new Buffer.from('{"msg_type":"connect"}', 'UTF-8'));
			} else {
				/* Something went wrong so start all over */
				console.log(`Couldn't find server with SID matching ${server_id}`);
				console.log("Finding server ...");
				client.send(discovery_message, 0, discovery_message.length, server_port, '255.255.255.255', function(err, bytes) { });
				setTimeout(choose_server, 1000, server_id);
			}
		} else {
			var choice = discovered_hosts[0];
			console.log(`Choosing server: ${choice.server_id} at ${choice.xk_server_address}`);
			server_addr = choice.xk_server_address;
			client.setBroadcast(false);
			send_udp_message(new Buffer.from('{"msg_type":"connect"}', 'UTF-8'));
		}
	}
}



/* This is where we start doing things:
*	- send DISCOVERY
*	- choose server from respondents
*	- send connect to chosen server
*	- if connect_result is OK, begin_normal_operations
*/

/*	Find the xkeys-server
*	See discovery.js for detail on how this works.
*/
var discovery_message = new Buffer.from('{"msg_type":"discover"}');
choose_server(target_serverId);


begin_normal_operations = () => {
	send_udp_message(JSON.stringify({msg_type:"device_layout", device:"XK12JOG"}));
}


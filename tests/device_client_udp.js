#!/usr/bin/env node


/*	device_client_udp.js
*
*	SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*	SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
* 
*	A client for testing communication with an DCD Protocol server using the
*	API described at https://gitlab.com/chris.willing/dcdp-server/-/tree/main/api
*
*	device_client.js [target] [logout]
*	where:
*		- target is name of a dcdp server
*		- logout (only valid if target is also specified)
*		  indicates whether or not to send disconnect message
*	Connects via UDP as a device client, send some device data (2x button presses),
*	then either:
*		- waits indefinitely (if logout not specified)
*		- sends device_disconnect message and terminates when
*		  msg_type:device_disconnect_result is received.
*/

const vendor_id = 1324;

let target_serverId;
const myArgs = process.argv.slice(2);
if (myArgs.length > 0) {
   target_serverId = myArgs[0];
}
var logout = false;
if (myArgs.length > 1) {
	if (myArgs[1] == "logout") {
		logout = true;
	}
}
console.log(`logout = ${logout}`);

const server_port = 48895;
let server_addr;
const discovered_hosts = [];

const dgram = require('dgram');
const client = dgram.createSocket('udp4');
client.bind( () => {
	client.setBroadcast(true);
});


client.on('message', (message, remote) => {
	//console.log('Msg Rcvd: ' + JSON.stringify(JSON.parse(message), null, 2) + "  from: " + JSON.stringify(remote));
	console.log(`Msg Rcvd: ${JSON.stringify(JSON.parse(message))} from: ${JSON.stringify(remote)})`);
	var msg = "";
	try{
		msg = JSON.parse(message);
		let msg_type;
		if (msg.hasOwnProperty('msg_type')) {
			msg_type = 'msg_type';
		} else {
			msg_type = 'request';
		}
		if (msg[msg_type] == "discover_result") {
			console.log(`Received a discover_result message. Server address: ${msg.xk_server_address}`);
			if (discovered_hosts.find(entry => { return entry.xk_server_address === msg.xk_server_address ; }) ) {
				/* We have already seen this server */
				console.log(`Not adding duplicate ${msg.xk_server_address}`);
			} else {
				console.log(`Adding server: ${JSON.stringify(msg)}`);
				discovered_hosts.push(msg);
			}
		} else if (msg[msg_type] == "connect_result") {
			console.log(`Received connect_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "list_attached_result") {
			var device_keys = Object.keys(msg.devices);
			if (device_keys.length > 0 ){
				console.log(JSON.stringify(msg.devices));
				/* Choose a device randomly to display */
				var choice = Math.floor(Math.random() * device_keys.length);
				console.log(`Showing number ${(choice + 1)} of ${device_keys.length} attached device(s) (${device_keys[choice]}):`);
				console.log(JSON.stringify(msg.devices[device_keys[choice]], null, 2));
			} else {
				console.log(`No devices attached at ${msg.server_id}`);
			}
		} else if (msg[msg_type] == "product_list_result") {
			var product_keys = Object.keys(msg.data);
			console.log(`Product List length = ${JSON.stringify(msg).length}`);
			if (product_keys.length > 0 ){
				/* Choose a device randomly to display */
				var choice = Math.floor(Math.random() * product_keys.length);
				console.log(`Showing number ${(choice + 1)} of ${product_keys.length} product(s):`);
				console.log(JSON.stringify(msg.data[product_keys[choice]]));
			} else {
				console.log(`No products available at ${msg.server_id}`);
			}
		} else if (msg[msg_type] == "command_result") {
			console.log(`Received command_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "list_clients_result") {
			console.log(`Received list_clients_result: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "disconnect_warning") {
			console.log(`Received disconnect_warning: ${JSON.stringify(msg)}`);
			console.log(`${Date.call()}`);
			setTimeout(send_udp_message, 10, (new Buffer.from('{"msg_type":"connect"}', 'UTF-8')));
		} else if (msg[msg_type] == "disconnect_result") {
			console.log(`Received disconnect_result: ${JSON.stringify(msg)}`);
			process.exit(0);
		} else if (msg[msg_type] == "device_disconnect_result") {
			console.log(`Received device_disconnect_result: ${JSON.stringify(msg)}`);
			process.exit(0);
		} else if (msg[msg_type] == "error") {
			console.log(`Received ERROR msg: ${JSON.stringify(msg)}`);
		} else if (msg[msg_type] == "button_event") {
			// Reflect test
			console.log(`Received REFLECTED message: ${JSON.stringify(msg)}`);
		} else if (/.*_event/.exec(msg[msg_type])) {
			/*	See all events */
			console.log(JSON.stringify(JSON.parse(message)));

			/* OR, to filter to see only "down" events
			*  - could be "up", "tbar", "jog", "shuttle", ...
			*
				if (msg.data.type == "down") {
					console.log(msg);
				}
			*/
		} else {
		}
	}
	catch (err) {
	}
});

send_udp_message = (message) => {
	/*	Before sending message, check that it's valid JSON */
	var msg = "";
	try {
		msg = JSON.parse(message);
	}
	catch (err) {
		console.log(`Not sending invalid message: ${message}`);
		console.log(err);
		return;
	}

	client.send(message, 0, message.length, server_port, server_addr, (err, bytes) => {
		if (err) {
			throw err;
		}
		//console.log(`Sending ${msg["msg_type"]} request to ${server_addr}:${server_port}`);
	});
}

choose_server = (server_id) => {
	if (discovered_hosts.length == 0) {
		/* No servers found so try again
		*/
		console.log("Finding server ...");
		try {
			client.setBroadcast(true);
		}
		catch (err) {
		}
		client.send(discovery_message, 0, discovery_message.length, server_port, '255.255.255.255', function(err, bytes) { });
		setTimeout(choose_server, 1000, server_id);
	} else {
		if (server_id) {
			/* Extract the entry with matching SID
			*/
			const target = discovered_hosts.find(entry => { return entry.server_id === server_id ; });
			if (target) {
				console.log(`Choice: ${JSON.stringify(target)}`);
				console.log(`Choice: ${target.server_id} at ${target.xk_server_address}`);
				server_addr = target.xk_server_address;
				client.setBroadcast(false);
				//send_udp_message(new Buffer.from('{"msg_type":"connect"}', 'UTF-8'));
				udp_message = {"msg_type":"device_connect", "device":"Remote Thing", "vendor_id":vendor_id, "product_id":"1404","unit_id":1, "rowCount":6,"colCount":4};
				send_udp_message(JSON.stringify(udp_message));
				//send_udp_message(new Buffer.from('{"msg_type":"device_connect", "device":"Remote Thing", "vendor_id":vendor_id, "product_id":"1404","unit_id":1, "rowCount":6,"colCount":4}', 'UTF-8'));
			} else {
				/* Something went wrong so start all over */
				console.log(`Couldn't find server with SID matching ${server_id}`);
				console.log("Finding server ...");
				client.send(discovery_message, 0, discovery_message.length, server_port, '255.255.255.255', function(err, bytes) { });
				setTimeout(choose_server, 1000, server_id);
			}
		} else {
			var choice = discovered_hosts[0];
			console.log(`Choosing server: ${choice.server_id} at ${choice.xk_server_address}`);
			server_addr = choice.xk_server_address;
			client.setBroadcast(false);
			//send_udp_message(new Buffer.from('{"msg_type":"connect"}', 'UTF-8'));
			make_message({"msg_type":"device_connect", "device":"Remote Thing", "vendor_id":vendor_id, "product_id":1404,"unit_id":1, "rowCount":6,"colCount":4});
			//send_udp_message(new Buffer.from('{"msg_type":"device_connect", "device":"Remote Thing", "vendor_id":vendor_id, "product_id":"1404","unit_id":1, "rowCount":6,"colCount":4}', 'UTF-8'));
		}
	}
}

/* Return value x normalized to range from min to max */
/*
normalize = (x, min, max) => {
    const fixlen = (max - min).toString().length
	return ((x-min)/(max - min)).toFixed(fixlen);
}
*/

/*	Find the xkeys-server
*	See discovery.js for detail on how this works.
*/
var discovery_message = JSON.stringify({msg_type:"discover"});
choose_server(target_serverId);


make_message = (x) => {
	console.log(`XXX x = ${JSON.stringify(x)}`);
	send_udp_message(JSON.stringify(x));
}


// Device Client Events
setTimeout(make_message, 3000, ({"msg_type":"device_data", "event_type":"button_event", "control_id":2, "row":2,"col":1,"value":1,"timestamp":730423776}));
setTimeout(make_message, 3100, ({"msg_type":"device_data", "event_type":"button_event", "control_id":2, "row":2,"col":1,"value":0,"timestamp":730423876}));

/*
setTimeout(make_message, 4000, ({"msg_type":"device_data", "event_type":"tbar_event", "control_id":0, "value":norm.normalize(51,"tbar"),"timestamp":730424776}));
setTimeout(make_message, 5000, ({"msg_type":"device_data", "event_type":"jog_event", "control_id":0, "value":norm.normalize(-1,"jog"),"timestamp":730425776}));
setTimeout(make_message, 6000, ({"msg_type":"device_data", "event_type":"shuttle_event", "control_id":0, "value":norm.normalize(7,"shuttle"),"timestamp":730426776}));
setTimeout(make_message, 7000, ({"msg_type":"device_data", "event_type":"joystick_event", "control_id":0, "x":norm.normalize(127,"joyx"),"y":norm.normalize(0,"joyy"),"z":norm.normalize(219,"joyx"),"deltaZ":norm.normalize(0,"joydeltaZ"),"timestamp":730427776}));
*/

log_me_out = () => {
	if (logout) {
		send_udp_message(JSON.stringify({msg_type:"device_disconnect"}));
	}
}
// Log out
setTimeout(log_me_out, 5100);

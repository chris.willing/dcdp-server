#!/usr/bin/env node

/*  discover-tcp.js
*
*   Discover the ip address & port of a machine offering
*   DCDP service. This example uses UDP for the service
*	discovery but any protocol of those offered may be used.
*	for connection.
*
*   In response to a msg_type:discover message, a reply with
*	msg_type:discover_result should contain a "services" field
*	- an array of available services e.g.
*	"services":[{"port":48895,"ifaddress":"192.168.68.128","type":"udp"},{"port":47795,"ifaddress":"192.168.68.128","type":"tcp"}]
*	which would denote availability DCDP service using both UDP
*	and TCP protocols at the given port numbers.
*
*   If no servers are discovered by initial service discovery,
*   we recheck discovery every 1 second.
*
*   If multiple servers are discovered we must decide which
*   to use. In this example, the first server found in
*   dcdp_services[] is chosen unless the desired server Id
*   is known and passed to discover-mdns.js as the first argument e.g.
*       ./discover-tcp.js pi4b
*/

const path = require('path');

/* Check whether a server Id is stipulated */
let target_serverId;
const myArgs = process.argv.slice(2);
if (myArgs.length > 0) {
   target_serverId = myArgs[0];
}

//const service_target = "udp";
// Extract the type of service (udp, tcp, etc.) from the script name
const service_target = path.basename(process.argv[1]).replace(/discover\-|\.js/g, '');
let discovered_server;

/*	Use UDP for service discovery
*/
const discovery_port = 48895;
var dgram = require("dgram");
var socket = dgram.createSocket("udp4");
socket.bind( () => {
        socket.setBroadcast(true);
});
const discover_message = new Buffer.from('{"msg_type":"discover"}');


/*	An array of suitable service(s) objects keyed by
*	the host (ip address) providing it/them.
*	Some hosts may provide multiple service types.
*
*	[ {"host0":[{s0},{s1},...]}, {"host1":[{s0},{s1},...]}, ... ]
*/
var dcdp_services = [];


/*  Discovery by UDP */
socket.on("message", (message, rinfo) => {
	const msg = JSON.parse(message);

	/* Check it's a message type we're interested in */
	if (msg["msg_type"] == "discover_result") {
		if (Object.keys(msg).includes("services")) {
			// Newer implementations include a "services" field
			msg.services.forEach( (service) => {
				//console.log(`service: ${JSON.stringify(service)}`);
				if (service.type == service_target) {
					//console.log(`considering service from ${msg.server_id}: ${JSON.stringify(service)}`);
					// Ignore if we have already seen this service from this server
					if (dcdp_services.find(entry => { return entry.ifaddress === service.ifaddress ; }) ) {
						//console.log(`Not adding duplicate address ${service.ifaddress}`);
					} else {
						var service_entry = service;
						service_entry["server_id"] = msg.server_id;
						dcdp_services.push(service_entry);
					}
				}
			});
		} else {
			console.log(`Response from ${msg.xk_server_address} has no 'services' field. Please update the server.`);
		}
	} else {
		/* Not interested in anything else */
	}
});


choose_server = (server_id) => {
	if (dcdp_services.length == 0) {
		/*	No servers found so try again.
		*/
		console.log("Finding DCDP servers ...");
		socket.send(discover_message, 0, discover_message.length, discovery_port, '255.255.255.255', function(err, bytes) { });
		setTimeout(choose_server, 1000, server_id);
	} else {
		//console.log(`dcdp_services: ${JSON.stringify(dcdp_services)}`);
	
		if (server_id) {
			console.log(`Search for ${server_id}`);
			// A server with desired server_id may have service at multiple interfaces
			const choices = [];
			dcdp_services.forEach( (entry) => {
				if (entry.server_id === server_id) {
					choices.push(entry);
				}
			});
			if (choices.length > 0) {
				// A real app should make more discerning choice. Here we choose the first found
				console.log(`Discovered DCDP server with ${service_target} service: ${choices[0].server_id} at ${choices[0].ifaddress}:${choices[0].port}`)
				process.exit(0);
			} else {
				/* Something went wrong so start all over */
				console.log(`Couldn't find server with SID matching ${server_id}`);
				console.log("Finding server ...");
				socket.send(discover_message, 0, discover_message.length, discovery_port, '255.255.255.255', function(err, bytes) { });
				setTimeout(choose_server, 1000, server_id);
			}
		} else {
			//console.log(`Choosing from: ${JSON.stringify(dcdp_services,null,2)}`);
			const choice = dcdp_services[0];
			console.log(`Discovered DCDP server with ${service_target} service: ${choice.server_id} at ${choice.ifaddress}:${choice.port}`);
			process.exit(0);
		}
	}
}

choose_server(target_serverId);

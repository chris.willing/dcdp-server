#!/usr/bin/env node

/*  connect-mdns-udp.js
*
*   Using mdns-sd, discover the ip address & port of a machine offering
*   DCDP service; then connect to it.
*
*	This example uses UDP but any protocol may be used.
*
*   If mdns-sd finds a suitable suitable server, a further
*       msg_type:"discover"
*   message is sent to the discovered address/port.
*   A reply with a field msg_type:"discover_result" is
*	expected and ,if received, connect to the service
*	(send msg_type:"connect").
*
*	In normal use, a client may connect directly following
*	discovery by mdsn-sd i.e. omit the msg_type:"discover"
*	message. However, since a discover_result message also
*	contains a list of attached devices, this may be useful
*	for deciding which of a number of servers to connect to.
*	It remains in this test client to exercise more server code.
*
*	This test also responds to periodic msg_type:disconnect_warning
*	messages by sending a new msg_type:connect message
*	(someday msg_type:disconnect_warning_result message?).
*
*   If multiple servers are discovered we must decide which
*   to use. In this example, the first server found in
*   dcdp_services[] is chosen unless the desired server Id
*   is known and passed to connect-mdns-udp.js as the first
*	argument e.g.
*       ./connect-mdns-udp.js pi4b
*
*	After connection to a specific (name on the command line,
*	as above) any number of commands may be sent to the server
*	e.g.
*		./connect-mdns-udp.js pi4b list_attached
*
*	Parameters for such commands can be edited in the script below.
*/

const mdns = require('mdns');

/* Check whether a server Id is stipulated */
let target_serverId;
device_commands = {};
const commands_to_run = [];
var command_type = "";

const discover_message = {msg_type:"discover"};
const connect_message = {msg_type:"connect", client_name:"Freddy"};
const disconnect_message = {msg_type:"disconnect"};
const list_attached_message = {msg_type:"list_attached"};
const all_product_data_message = {msg_type:"all_product_data"};
const list_products_message = {msg_type:"list_products", vendor_id: [4057, 1523]};
const product_details_message = {msg_type:"product_details", name:["XK24","SDMINI"]};
const list_clients_message = {msg_type:"list_clients"};
const amessage = {msg_type:"button_event", server_id:"MHHDELL", device:"XKE124TBAR",
				vendor_id:1523, product_id:1278, unit_id:1, duplicater_id:0,
				control_id:8, row:8, col:1, value:0, timestamp:730421776};
const reflect_message = {msg_type:"reflect", message:amessage};

/*	These are messages for the various device commands
*	with some default paramaters which can be changed
*	as required.
*/

device_commands["list_attached"] = list_attached_message;
device_commands["all_product_data"] = all_product_data_message;
device_commands["list_products"] = list_products_message;
device_commands["product_details"] = product_details_message;
device_commands["list_clients"] = list_clients_message;
device_commands["reflect"] = reflect_message;

device_commands["set_flash_rate"] = {msg_type:"command", command_type:"set_flash_rate",
									vendor_id: 1523, product_id:1029, unit_id:7, duplicate_id:0,
									value:40
};
device_commands["set_backlight_intensity"] = {msg_type:"command", command_type:"set_backlight_intensity",
									vendor_id: 1523, product_id:1029, unit_id:5, duplicate_id:0,
									value:255
};
device_commands["set_all_backlights"] = {msg_type:"command", command_type:"set_all_backlights",
									vendor_id: 1523, product_id:1029, unit_id:5, duplicate_id:0,
									value:1, color:"blue"
};
device_commands["set_backlight"] = {msg_type:"command", command_type:"set_backlight",
									vendor_id: 1523, product_id:1029, unit_id:7, duplicate_id:0,
									control_id:[1,2,3,4,5,6,7,8,9,10,11,12], value:1, color:"red", flash:1
};
device_commands["set_indicator_led"] = {msg_type:"command", command_type:"set_indicator_led",
									vendor_id: 1523, product_id:-1, unit_id:-1, duplicate_id:-1,
									control_id:2, value:1, flash:0
};
device_commands["write_lcd_display"] = {msg_type:"command", command_type:"write_lcd_display",
									vendor_id: 1523, product_id:1321, unit_id:5, duplicate_id:-1,
									line: [1,2], text:["Struth,","It's Freddy's friend!"], backlight: 1
};
device_commands["write_data"] = {msg_type:"command", command_type:"write_data",
									vendor_id: 1523, product_id:1029, unit_id:5, duplicate_id:0,
									byte_array:[0, 184]
};
device_commands["set_unit_id"] = {msg_type:"command", command_type:"set_unit_id",
									vendor_id: 1523, product_id:1029, unit_id:2, duplicate_id:0,
									new_unit_id:4
};
device_commands["save_backlight"] = {msg_type:"command", command_type:"save_backlight",
									vendor_id: 1523, product_id:1029, unit_id:5, duplicate_id:0
};

const myArgs = process.argv.slice(2);
if (myArgs.length > 0) {
	target_serverId = myArgs[0];
	console.log(`Targeting server ${target_serverId}`);

	for (var i=1;i<myArgs.length;i++) {
		if (device_commands.hasOwnProperty(myArgs[i])) {
			console.log(`Adding command: ${myArgs[i]}`);
			commands_to_run.push(myArgs[i]);
		}
	}
}

var dcdp_services = [];

/*  Set up service discovery */
const sequence = [
    mdns.rst.DNSServiceResolve(),
    "DNSServiceGetAddrInfo" in mdns.dns_sd ? mdns.rst.DNSServiceGetAddrInfo() : mdns.rst.getaddrinfo({families: [4]}),
	mdns.rst.makeAddressesUnique(),
];
const browser = mdns.createBrowser(mdns.makeServiceType('dcdp', 'udp'), {resolverSequence: sequence});

browser.on('serviceUp', service => {
	if (!service.txtRecord) return;
	if (!service.txtRecord.oaddr) return;
	console.log("service up: ", service.txtRecord);
	service_duplicate = false;
	dcdp_services.forEach((service_entry) => {
		if (service_entry.oaddr == service.txtRecord.oaddr) {
			//console.log(`duplicate oaddress: ${service.txtRecord.oaddr}`);
			service_duplicate = true;
		}
	});
	if (service_duplicate) {
		console.log(`Duplicate service advertisement from: ${service.txtRecord.oaddr}`);
	} else {
		dcdp_services.push(service.txtRecord);
	}
});
browser.on('serviceDown', service => {
  console.log("service down: ", service);
});
browser.on('error', exception => {
  console.log("service error: ", exception.toString());
});
browser.start();


//setTimeout(() => {console.log(`======== Available services =======`);console.log(`service: ${JSON.stringify(dcdp_services,null,2)}`);}, 1000);


var dgram = require("dgram");
var socket = dgram.createSocket("udp4");
socket.bind( () => {
       socket.setBroadcast(true);
});

let service_chosen;
var command_timeout = 0;

socket.on("message", (message, rinfo) => {
	const msg = JSON.parse(message);
	let msg_type;
	if (msg.hasOwnProperty('msg_type')) {
		msg_type = 'msg_type';
	} else {
		msg_type = 'request';
	}
	/* Check it's a message type we're interested in */
	if (msg[msg_type] == "discover_result") {
		console.log(`Found server to use: ${JSON.stringify(msg)}`);

		//Now try connecting to it
		const msg_str = JSON.stringify(connect_message);
		socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });

	} else if (msg[msg_type] == "connect_result") {
		console.log(`rcvd connect_result: ${message}`);

		let counter = 0;
		for (var i=0;i<commands_to_run.length;i++) {
			command_type = commands_to_run[i];
			if (device_commands.hasOwnProperty(command_type)) {
				setTimeout(() => {
					setTimeout(() => {
						console.log(`Running device command: ${JSON.stringify(device_commands[commands_to_run[counter]])}`);
						const msg_str = JSON.stringify(device_commands[commands_to_run[counter]]);
						socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
						counter += 1;
					}, 1000);
				}, command_timeout);
				command_timeout += 4000;
			}
		}

		//	Disconnect
		setTimeout(() => {
			console.log(`Disconnecting after timeout`);
			const msg_str = JSON.stringify(disconnect_message);
			socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
		}, (command_timeout));

	} else if (msg[msg_type] == "list_attached_result") {
		//console.log(`rcvd list_attached_result: ${message}`);
		msg.devices.forEach( (device) => {
			console.log(`device ${device.device_quad} ${device.name}`);
			if (device.name == "XK-24" ) {
				console.log(`XK-24: ${JSON.stringify(device)}`);
			}
		});

	} else if (msg[msg_type] == "all_product_data_result") {
		console.log(`rcvd all_product_data_result: ${message}`);

	} else if (msg[msg_type] == "list_products_result") {
		console.log(`rcvd list_products_result: ${message}`);

	} else if (msg[msg_type] == "product_details_result") {
		console.log(`rcvd product_details_result: ${message}`);

	} else if (msg[msg_type] == "list_clients_result") {
		console.log(`rcvd list_clients_result: ${message}`);

	} else if (msg[msg_type] == "button_event") {
		console.log(`rcvd button_event: ${message}`);

	} else if (msg[msg_type] == "disconnect_warning") {
		console.log(`rcvd disconnect_warning: ${message}`);

		// Send a connect message to maintain connection.
		const msg_str = JSON.stringify(connect_message);
		socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });

	} else if (msg[msg_type] == "disconnect_result") {
		console.log(`rcvd disconnect_result: ${message}`);
		socket.close();
		process.exit(0);

	} else if (msg[msg_type] == "command_result") {
		console.log(`rcvd command_result: ${message}`);

	} else {
		/* Not interested in anything else */
	}
});


choose_server = (server_id) => {
	if (dcdp_services.length == 0) {
		/*	No servers found so try again.
		*/
		console.log("Finding server ...");
		setTimeout(choose_server, 1000, server_id);
	} else {
	
		if (server_id) {
			console.log(`Search for ${server_id}`);
			dcdp_services.forEach((service_entry) => {
				if (service_entry.oid == server_id) {
					service_chosen = service_entry;
					console.log(`Have requested server_id (${server_id})`);
					const msg_str = JSON.stringify(discover_message);
					socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
				}
			});
		} else {
			console.log(`Choosing from: ${JSON.stringify(dcdp_services,null,2)}`);
			service_chosen = dcdp_services[0];
			const msg_str = JSON.stringify(discover_message);
			socket.send(msg_str, 0, msg_str.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
		}
	}
}

choose_server(target_serverId);

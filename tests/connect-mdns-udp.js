#!/usr/bin/env node

/*  connect-mdns-udp.js
*
*   Using mdns-sd, discover the ip address & port of a machine offering
*   DCDP service; then connect to it.
*
*	This example uses UDP but any protocol may be used.
*
*   If mdns-sd finds a suitable suitable server, a further
*       "msg_type":"discover"
*   message is sent to the discovered address/port.
*   A reply with a field "msg_type":"discover_result" is
*	expected and ,if received, connect to the service
*	(send "msg_type":"connect").
*
*	In normal use, a client may connect directly following
*	discovery by mdsn-sd i.e. omit the "msg_type":"discover"
*	message. However, since a discover_result message also
*	contains a list of attached devices, this may be useful
*	for deciding which of a number of servers to connect to.
*	It remains in this test client to exercise more server code.
*
*	This test also responds to periodic msg_type:disconnect_warning
*	messages by sending a new msg_type:connect message
*	(someday msg_type:disconnect_warning_result message?).
*
*	After some time of connection, this client sends a
*	"msg_type":"list_attached" message. The server should respond
*	with a msg_type:list_attached_result message.
*
*	After some more time of connection, this client sends a
*	"msg_type":"all_product_data" message. The server should respond
*	with a msg_type:all_product_data_result message.
*
*	After some time (25s) of connection, this client sends a
*	"msg_type":"disconnect" message. The server should respond with
*	a "msg_type":"disconnect_result" message after which
*	(presuming it is received) this client terminates.
*
*   If multiple servers are discovered we must decide which
*   to use. In this example, the first server found in
*   dcdp_services[] is chosen unless the desired server Id
*   is known and passed to connect-mdns-tcp.js as the first
*	argument e.g.
*       ./connect-mdns-udp.js pi4b
*/

const mdns = require('mdns');

/* Check whether a server Id is stipulated */
let target_serverId;
const myArgs = process.argv.slice(2);
if (myArgs.length > 0) {
   target_serverId = myArgs[0];
}

var dcdp_services = [];

/*  Set up service discovery */
const sequence = [
    mdns.rst.DNSServiceResolve(),
    "DNSServiceGetAddrInfo" in mdns.dns_sd ? mdns.rst.DNSServiceGetAddrInfo() : mdns.rst.getaddrinfo({families: [4]}),
	mdns.rst.makeAddressesUnique(),
];
const browser = mdns.createBrowser(mdns.makeServiceType('dcdp', 'udp'), {resolverSequence: sequence});

browser.on('serviceUp', service => {
	if (!service.txtRecord) return;
	if (!service.txtRecord.oaddr) return;
	console.log("service up: ", service.txtRecord);
	service_duplicate = false;
	dcdp_services.forEach((service_entry) => {
		if (service_entry.oaddr == service.txtRecord.oaddr) {
			//console.log(`duplicate oaddress: ${service.txtRecord.oaddr}`);
			service_duplicate = true;
		}
	});
	if (service_duplicate) {
		console.log(`Duplicate service advertisement from: ${service.txtRecord.oaddr}`);
	} else {
		dcdp_services.push(service.txtRecord);
	}
});
browser.on('serviceDown', service => {
  console.log("service down: ", service);
});
browser.on('error', exception => {
  console.log("service error: ", exception.toString());
});
browser.start();


//setTimeout(() => {console.log(`======== Available services =======`);console.log(`service: ${JSON.stringify(dcdp_services,null,2)}`);}, 1000);


var dgram = require("dgram");
var socket = dgram.createSocket("udp4");
socket.bind( () => {
       socket.setBroadcast(true);
});

let service_chosen;
var discover_message = new Buffer.from('{"msg_type":"discover"}');
var connect_message = new Buffer.from('{"msg_type":"connect", "client_name":"Freddy"}');
var disconnect_message = new Buffer.from('{"msg_type":"disconnect"}');
var list_attached_message = new Buffer.from('{"msg_type":"list_attached"}');
var all_product_data_message = new Buffer.from('{"msg_type":"all_product_data"}');

socket.on("message", (message, rinfo) => {
	const msg = JSON.parse(message);
	let msg_type;
	if (msg.hasOwnProperty('msg_type')) {
		msg_type = 'msg_type';
	} else {
		msg_type = 'request';
	}
	/* Check it's a message type we're interested in */
	if (msg[msg_type] == "discover_result") {
		console.log(`Found server to use: ${JSON.stringify(msg)}`);

		//Now try connecting to it
		socket.send(connect_message, 0, connect_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });

	} else if (msg[msg_type] == "connect_result") {
		console.log(`rcvd connect_result: ${message}`);

		// After some time, send msg_type:list_attached
		setTimeout(() => {
		   console.log(`Requesting attached device list`);
			socket.send(list_attached_message, 0, list_attached_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
		}, 10000);

		// After some time, send msg_type:all_product_data
		setTimeout(() => {
		   console.log(`Requesting product list`);
			socket.send(all_product_data_message, 0, all_product_data_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
		}, 15000);


		// After some time, send disconnect message
        setTimeout(() => {
            console.log(`Disconnecting after timeout`);
			socket.send(disconnect_message, 0, disconnect_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
        }, 25000);

	} else if (msg[msg_type] == "list_attached_result") {
		console.log(`rcvd list_attached_result: ${message}`);

	} else if (msg[msg_type] == "all_product_data_result") {
		console.log(`rcvd all_product_data_result: ${message}`);

	} else if (msg[msg_type] == "disconnect_warning") {
		console.log(`rcvd disconnect_warning: ${message}`);

		// Send a connect message to maintain connection.
		socket.send(connect_message, 0, connect_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });

	} else if (msg[msg_type] == "disconnect_result") {
		console.log(`rcvd disconnect_result: ${message}`);
		socket.close();
		process.exit(0);

	} else {
		/* Not interested in anything else */
	}
});


choose_server = (server_id) => {
	if (dcdp_services.length == 0) {
		/*	No servers found so try again.
		*/
		console.log("Finding server ...");
		setTimeout(choose_server, 1000, server_id);
	} else {
	
		if (server_id) {
			console.log(`Search for ${server_id}`);
			dcdp_services.forEach((service_entry) => {
				if (service_entry.oid == server_id) {
					service_chosen = service_entry;
					console.log(`Have requested server_id (${server_id})`);
					socket.send(discover_message, 0, discover_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
				}
			});
		} else {
			console.log(`Choosing from: ${JSON.stringify(dcdp_services,null,2)}`);
			service_chosen = dcdp_services[0];
			socket.send(discover_message, 0, discover_message.length, service_chosen.oport, service_chosen.oaddr, function(err, bytes) { });
		}
	}
}

choose_server(target_serverId);

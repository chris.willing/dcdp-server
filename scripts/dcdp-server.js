

const fs = require('fs');
const path = require('path');
const usbDetect = require('usb-detection');
const crypto = require('crypto');
process.env.UV_THREADPOOL_SIZE = 48;

const ServerVersion = require('../package.json').version;
console.log(`Server version = ${ServerVersion}`);
console.log(`NodeJS version = ${process.version}`);

const { networkInterfaces } = require('os');
const ServerID = "DCD_" + require('os').hostname().split('.')[0];
//const ServerID = "DCD_Chris' test UDP server"; 
console.log(`Server ID = ${ServerID}`);

const xkeysLayouts = require('xkeys-layouts');
//console.log(`xkeys_layouts: ${xkeysLayouts.testing()}`);


/*	A list of client connections is maintained in a separate module
*/
const ClientConnections = require('./ClientConnections');
//	TODO! Derive these from configuration
const TIMEOUT_CLIENT_TTL = 10000;
const TIMEOUT_CLIENT_WARNING_TTL = 2000;
const CLIENT_TTL_WARNINGS = 4;
/*	Use configured values to instantiate new ClientConnections */
//let client_connections = new ClientConnections({client_ttl:TIMEOUT_CLIENT_TTL, client_warning_ttl:TIMEOUT_CLIENT_WARNING_TTL, ttl_warnings:CLIENT_TTL_WARNINGS});
/*	OR use default values set in ClientConnections.js */
let client_connections = new ClientConnections({});

/*	hw is an object containing:
*	- reference (by vendor name) to a supported hardware module (as imported with require())
*	  e.g. "xkeys":<module>
*	- reference (by vendorId) to a supported hardware module (as imported with require())
*	  e.g. "1529":<module>
*
*	This enables a particular module's functions to be called by something like:
*		console.log(`detected: ${hw[device.vendorId].testing()}`);
*/
let hw = {};
let transports = {};

/*	Record of all the device objects known to the server,
*	keyed by each device's "device-quad" identifier.
*/
// previously xkeys_devices in xkeys-server
let ServerDevices = {};

/*	Some device clients do not respond to  timeout warnings,
*	therefore possibly sending button_events etc. after they have been
*	disconnected. At disconnection, keep a copy of the device details
*	so that such device clients can be identified and reconnected.
*/
let LostDeviceClients = {};

let AllProducts = {};
let ProductSummaries = {};

/*	Record of different manufacturers' device layouts
*	(only X-keys, so far).
*/
summary_data_all_xkeys = xkeysLayouts.summary();
var xkeys_layouts_devices = [];
Object.keys(summary_data_all_xkeys).forEach( (device_name) => {
	xkeys_layouts_devices.push(device_name);
});
console.log(`xkeys_layouts_devices: ${xkeys_layouts_devices}`);


var config = {};
config["hw_types"] = [];
config["transport_types"] = [];
/*	TODO!
*	Enable these by configuration
*/
config.transport_types.push("udp");
config.transport_types.push("tcp");
config.transport_types.push("mqtt");

/*	Find supported hardware modules
*	passed from environment
*	(as something like DCDPS_HW=X,Y,Z)
*/
var hw_passed = [];
const default_hardware_types = ["xkeys", "streamdeck"];
if (process.env.DCDPS_HW) { hw_passed = process.env.DCDPS_HW.split(','); }
/* FOR NOW: Ignore setting hw types from environment
var  hw_dir = fs.readdirSync(path.relative(process.cwd(), path.join(__dirname, 'hw')));
hw_dir.forEach( (hw_item) => {
	//console.log(`Hardware item (1): ${hw_item}`);
	if (hw_item.startsWith('HW_') && hw_item.endsWith('.js')) {
		var hw_type = hw_item.replace(/^HW_|.js$/g, '');
		//if (hw_type == "xkeys") {
		if (default_hardware_types.includes(hw_type)) {
			// Always use xkeys (default hardware type)
			console.log(`Adding default hardware: ${hw_type}`);
			config.hw_types.push(hw_type);

		} else if (hw_passed.includes(hw_type)) {
			config.hw_types.push(hw_type);

		} else {
			//console.log(`${hw_passed} doesn't include ${hw_type}`);
		}

	} else {
		console.log(`not: ${hw_item}`);
	}
})
*/
// Just use the defaults for now
default_hardware_types.forEach( (hw_type) => {
	config.hw_types.push(hw_type);
});
console.log(`Configuration: ${JSON.stringify(config)}`);

/*	Log if module for hardware requested in environment is not available? */
hw_passed.forEach( (passed_item) => {
	if (!config.hw_types.includes(passed_item)) {
		console.log(`Couldn't find module for requested hardware: ${passed_item}`);
	}
});


server_id = () => {
	return ServerID;
}
server_devices = () => {
	return ServerDevices;
}
/*	send_message (message, ...dest)
*
*	If only a message is provided, it is sent to all known clients.
*	If additional args are provided, the msg is sent to the particular
*	destination specified. It is provided as a remoteInfo object whose
*	contents may differ depending on the transport type but must, at
*	least, contain the transport type to be used e.g.
*		{type:"mqtt"}
*	or
*		{type:"udp", address:"192.168.3.45", port:32765}
*/
send_message = (message, ...dest) => {
	//console.log(`send_message() with dest: ${JSON.stringify(dest[0])}`);

	if (dest.length == 0) {
		// Most common case - send to all clients.
		//console.log(`send_message() ${message} to all connected clients`);

		//console.log(`client_connections.list: ${JSON.stringify(client_connections.list())}`);
		client_connections.list().forEach( (client) => {
			//console.log(`transport: ${JSON.stringify(client.remote.transport)}`);
			if (config.transport_types.includes(client.remote.transport)) {
				transports[client.remote.transport].send(message, client.remote);
			} else {
				console.log(`send_message() unknown transport requested (${client.remote.transport})`);
			}
		})
		// All includes mqtt (whose subscribers are not part of client_connections)
		if (config.transport_types.includes("mqtt")) {
			transports["mqtt"].send(message);
		}
	} else {
		// A particular rinfo of address,port and/or transport type have been specified.
		//console.log(`send_message() ${message} to specific client`);
		if (Object.keys(transports).includes(dest[0].transport)) {
			transports[dest[0].transport].send(message, dest[0]);
		} else {
			console.log(`send_message() - unknown transport: ${dest[0].transport}`);
		}
	}
}

/*	server_add_device(device)
*
*	Add a newly discovered device to ServerDevices object.
*
*	The device must include (actual or synthesized) vendorId & uniqueId properties.
*	Here we generate a "duplicate_id" property to correctly distinguish between
*	multiple similar looking devices i.e. have same vendor & same product & same unit_id.
*
*	We then compose a "device-quad" (using vendor_id, product_id, unit_id & duplicate_id)
*	to be used as the key when adding this new device to the xkeys_devices object.
*/
// was add_xkeys_device()
server_add_device = (device) => {
	//console.log(`server_add_device(): ${JSON.stringify(device)}`);
	//console.log(`server_add_device() INFO: ${JSON.stringify(device.info)}`);
	//console.log(`server_add_device() INFO productId: ${device.info.productId}`);

	/*	Generate duplicate_id */
	var duplicate_id = 0;
	var temp_id_base = device.vendorId.toString() + '-' + device.uniqueId.replace(/_/g, "-");
	while ((temp_id_base + '-' + duplicate_id) in ServerDevices) {
		duplicate_id += 1;
	}
	device["duplicate_id"] = duplicate_id.toString();

	/*	Compose the device-quad */
	var temp_id = temp_id_base + "-" + device.duplicate_id;
	//console.log(`New device entry: ${temp_id}`);
	ServerDevices[temp_id] = device;

	console.log(`After server_add_device(): ServerDevices = ${JSON.stringify(Object.keys(ServerDevices))}`);
}	// END function server_add_device()

/*	server_remove_device(temp_id)
*
*	Remove the ServerDevices entry keyed by temp_id.
*/
server_remove_device = (temp_id) => {
	console.log(`Removing device panel ${temp_id}`);
	delete ServerDevices[temp_id];
}	// END server_remove_device()

/*	Load hardware modules
*/
/*	Dynamic require at run time is a problem for pkg
*	so hard code hardware modules for now.
*/
hw["xkeys"] = require("./hw/HW_xkeys");
hw["streamdeck"] = require("./hw/HW_streamdeck");

if (config.hw_types.length == 0) {
	console.log("No hardware available. Exiting now ...");
	process.exit(1);
} else {
	config.hw_types.forEach( (hw_type) => {
		//console.log(`Loading hw_type: ${hw_type}`);
		//hw[hw_type] = require("./hw/HW_" + hw_type);

		/*	Reference the newly loaded module by vendorId of that module */
		hw[hw[hw_type].vendorId()] = hw[hw_type];

		/*	Start the module (registers currently connected hardware) */
		hw[hw_type].start();
		//	Setup products lists
		hw[hw_type].products(AllProducts, ProductSummaries);
	})
}
//console.log(`ProductSummaries = ${JSON.stringify(ProductSummaries)}`);
//console.log(`AllProducts = ${JSON.stringify(AllProducts)}`);
//console.log(`hw = ${JSON.stringify(hw)}`);

/* Load available message transports
*/
/*	Dynamic require at run time is a problem for pkg
*	so hard code hardware modules for now.
*/
transports["udp"] = require("./net/NW_udp");
transports["tcp"] = require("./net/NW_tcp");
transports["mqtt"] = require("./net/NW_mqtt");

config.transport_types.forEach( (t_type) => {
	console.log(`Loading transport type: ${t_type}`);
	//transports[t_type] = require("./net/NW_" + t_type);

	transports[t_type].testing();
	transports[t_type].start();
});

/*	Test modules loaded OK
config.hw_types.forEach( (hw_type) => {
	try {
		console.log(`Testing module: ${hw_type}`);
		hw[hw_type].testing();
	}
	catch (err) {
		console.log(`CAUGHT ERROR  ${err}`);
	}
})
*/

/*	advertise_dcdp_server()
*
*	On each network interface, advertise the available
*	services (message transport types).
*
*	At 5 second intervals, each transport is instructed
*	to check whether a restart of advertising is needed.
*/
var mdns_needs_restart = true;
function advertise_dcdp_services () {
	//console.log(`advertise_dcdp_services()`);
	setTimeout(advertise_dcdp_services, 5000);

	Object.keys(transports).forEach( (transport) => {
		transports[transport].check_mdns(ServerID);
	})
}
advertise_dcdp_services();

process.on('SIGINT', () => {
	console.log(`Shutting down (SIGINT)`);
	usbDetect.stopMonitoring();
	//udp_server.close();
	//client.end();
	config.hw_types.forEach( (hw_type) => {
		try {
			hw[hw_type].stop();
		}
		catch (err) {
			console.log(`CAUGHT ERROR  ${err}`);
		}
	})

	process.exit();
});
process.on('SIGTERM', () => {
	console.log(`Shutting down (SIGTERM)`);
	usbDetect.stopMonitoring();
	//udp_server.close();
	//client.end();
	config.hw_types.forEach( (hw_type) => {
		try {
			hw[hw_type].stop();
		}
		catch (err) {
			console.log(`CAUGHT ERROR  ${err}`);
		}
	})

	process.exit();
});


usbDetect.on('add', function(device) {
	//console.log(`usbDetect add device: ${JSON.stringify(device)}`);
	if (hw.hasOwnProperty(device.vendorId)) {
		try {
			hw[device.vendorId].add(device);
		}
		catch (err) {
			console.log(`usbDetect.on('add'): ${err}`);
		}
	}
})
usbDetect.on('remove', function (device) {
	//console.log(`${JSON.stringify(device)} was removed`);
	if (hw.hasOwnProperty(device.vendorId)) {
		hw[device.vendorId].remove(device);
	}
})
usbDetect.startMonitoring();

add_client_connection = (connection) => {
	//console.log(`add_client_connection()`);
	client_connections.add(connection);
};
check_client_connection = (connection) => {
	//console.log(`check_client_connection()`);
	client_connections.send_ttl_warning(connection);
};
reset_client_connection_timer = (connection) => {
	client_connections.reset_timer(connection);
};
remove_client_connection = (connection) => {
	//console.log(`remove_client_connection() ${JSON.stringify(connection)}`);

	//	First, if this is a device client connection, remove the associated device
	//	However first save device details for possible reuse e.g. recreate a device client which has lost connection.
	const index = client_connections.list().findIndex(item => item.remote.address === connection.address && item.remote.port === connection.port && item.hasOwnProperty('device_quad'));
	if (index >= 0) {
		// Insert device name into client object to include in device_disconnect_result message
		client_connections.list()[index]["device"] = ServerDevices[client_connections.list()[index].device_quad].shortnam;

		// Keep details of removed device client in LostDeviceCllients object.
		const lost_device_client = JSON.parse(JSON.stringify(ServerDevices[client_connections.list()[index].device_quad]));
		LostDeviceClients[client_connections.list()[index]["client_name"]] = {};
		LostDeviceClients[client_connections.list()[index]["client_name"]]["connection"] = connection;
		LostDeviceClients[client_connections.list()[index]["client_name"]]["device"] = lost_device_client;
		server_remove_device(client_connections.list()[index].device_quad);
	}
	client_connections.remove(connection);
};
show_client_connections = () => {
	//console.log(`show_client_connections():`);
	for (const client of client_connections.list()) {
		const client_display = {"client_name":client.client_name, "address":client.remote.address, "port":client.remote.port, "transport":client.remote.transport, "warnings":client.warnings};
		console.log(`show_client_connections(): ${JSON.stringify(client_display)}`);
	}
}

/*	request_message_process()
*
*	Universal request processor, through which messages
*	from all transport types (mqtt, udp, ...) pass.
*
*	type    : string describing caller's transport ("mqtt"|"udp"|...)
*	message : JSON format string
*	moreArgs: array of additional args
*		moreArgs[0] is the topic (string) of an MQTT message
*		moreArgs[0] is the remoteInfo (object) of a UDP or TCP message
*/
request_message_process = (message, remoteInfo) => {
	var msg_transport = undefined;
	var topic = undefined;
	if (typeof(remoteInfo) == 'string') {
		topic = remoteInfo
		msg_transport = "mqtt";
	} else {
		//console.log(`remoteInfo: ${JSON.stringify(remoteInfo)}`);
		msg_transport = remoteInfo.transport;
	}

	var msg = "";
	if (["udp", "tcp", "mqtt"].includes(msg_transport)) {

		/* Basic syntax check */
		try {
			msg = JSON.parse(message);
			if (! msg.hasOwnProperty('msg_type')) {
				// UDP/TCP messages MUST have this field
				console.log(`UDP/TCP message without msg_type rejected`);
				send_message(JSON.stringify({msg_type:"error",server_id:ServerID, error_msg:"Illegal message format: 'msg_type' is missing", error_echo:message}), remoteInfo);
				return;
			}
		}
		catch (err) {
			// Probably a JSON syntax error
			console.log(`Sending error_msg for message: ${message.toString()}`);
			send_message(JSON.stringify({msg_type:"error",server_id:ServerID, error_msg:"" + err, error_echo:message.toString()}), remoteInfo);
			return;
		}
		/*	Process the message */
		msg = JSON.parse(message);
		reset_client_connection_timer(remoteInfo);
		try {
			switch (msg["msg_type"]) {
				case "discover":
					console.log(`Processing discovery request`);

					/*	Since we exist on 0.0.0.0 i.e. every available interface,
					*	and therefore possibly have multiple IP addresses,
					*	find the best IP address to provide the client with.
					*/
					const nifs = networkInterfaces();
					const nif_addrs = {};
					for (const name of Object.keys(nifs)) {
						for (const net of nifs[name]) {
							// Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
							if (net.family === 'IPv4' && !net.internal) {
								if (!nif_addrs[name]) {
									nif_addrs[name] = [];
								}
								nif_addrs[name].push(net.address);
							}
						}
					}
					//console.log(`${JSON.stringify(nif_addrs)}`);
					var address_match = "";
					for (const name of Object.keys(nif_addrs)) {
						nif_addrs[name].forEach( (item) => {
							//console.log(`Checking: ${item}, against ${remoteInfo.address}`);
							if (item == remoteInfo.address) {
								address_match = item;
								return;
							} else if (/[0-9]*\.[0-9]*\.[0-9]*/.exec(item).toString() == /[0-9]*\.[0-9]*\.[0-9]*/.exec(remoteInfo.address).toString()) {
								address_match = item;
								return;
							} else if (/[0-9]*\.[0-9]*/.exec(item).toString() == /[0-9]*\.[0-9]*/.exec(remoteInfo.address).toString()) {
								address_match = item;
								return;
							} else {
							}
						});
						if (address_match.length > 0) {
							//console.log(`The match is: ${address_match}`);
							break;
						}
					}
					if (address_match.length < 7) {
						return;
					}

					var discover_result = {msg_type:"discover_result", server_id:ServerID};
					discover_result["xk_server_address"] = address_match;
					var services = [];
					Object.keys(transports).forEach( (t_type) => {
						const service_info = transports[t_type].services();
						service_info.forEach( (service) => {
							service["type"] = t_type;
						});
						console.log(`Service Info: ${JSON.stringify(service_info)}`);
						services = services.concat(service_info);
					});
					console.log(`Combined services = ${JSON.stringify(services)}`);
					discover_result["services"] = services;
					discover_result["attached_devices"] = Object.keys(server_devices());
					discover_result["version"] = ServerVersion;
					//console.log(`sending discover_result message with remoteInfo: ${JSON.stringify(remoteInfo)}`);

					send_message(JSON.stringify(discover_result), remoteInfo);

				break;

				case "connect":
					/*	Normal client connect */
					//	Start building a connect_result message
					var connect_result = {msg_type:"connect_result", server_id:ServerID};
					connect_result["client_address"] = remoteInfo.address;
					connect_result["client_port"] = remoteInfo.port;

					/*	Determine new or returning client.
					*	A new client may not ask for a particular name (allocate a name).
					*	A returning client may be requesting a new name.
					*	In any case, don't allow duplicate names.
					*/
					const index = client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port);
					if (index < 0) {
						//	New client
						if (msg.hasOwnProperty("client_name")) {
							//	Requesting a name - check that it's available
							const name_used = client_connections.list().findIndex(item => item.client_name === msg.client_name);
							if (name_used > -1 ) {
								//	Request name already in use. Allocate a new name.
								msg["client_name"] = msg["client_name"] + "_" + crypto.randomBytes(8).toString('hex');
							}
						} else {
							//	No name requested - allocate something
							msg["client_name"] = "client_" + crypto.randomBytes(8).toString('hex');
						}
						add_client_connection({"timestamp":Date.now(), "client_name":msg.client_name, "remote":remoteInfo});
					} else {
						//	Existing client returning
						if (msg.hasOwnProperty("client_name")) {
							//	Requesting a name - check that it's available
							const name_used = client_connections.list().findIndex(item => item.client_name === msg.client_name);
							if (name_used > -1 ) {
								//	Requested name already in use.
								//	If already used by the same client returning, do nothing.
								//	If being used by another client, we'll have to change our name.
								if ((client_connections.list()[name_used].remote.address == remoteInfo.address) && (client_connections.list()[name_used].remote.port == remoteInfo.port)) {
									//	Nothing to do - requested name is already owned by us.
								} else {
									//	Another client is already using this name. We have to choose another name.
									msg["client_name"] = msg["client_name"] + "_" + crypto.randomBytes(8).toString('hex');

								}
							} else {
								//	Requested name not yet used so keep using it.
							}
						} else {
							// No new name requested so use existing
							msg.client_name = client_connections.list()[index].client_name;
						}
						add_client_connection({"timestamp":Date.now(), "client_name":msg.client_name, "remote":remoteInfo});
					}

					//	Remainder of connect_result message.
					connect_result["client_name"] = msg.client_name;
					connect_result["attached_devices"] = Object.keys(server_devices());
					connect_result["version"] = ServerVersion;

					//	The connect_result message is sent to all connected clients
					//	so everyone can know who is connected
					send_message(JSON.stringify(connect_result));
				break;

				case "disconnect":
					/*	Normal client disconnect
					*
					*	No need to build & send a disconnect_result message from here.
					*	It will be sent automatically when client is removed from client_connections.
					*/
					const client_name = client_connections.name(remoteInfo);
					if (client_name) {
						//console.log(`case disconnect: disconnecting ${client_name}`);
						remove_client_connection(remoteInfo);
					} else {
						console.log(`case disconnect: client_name not found`);
						return;
					}
				break;

				case "device_connect":
				{	// Fresh scope device_connect
					/*	This is a "Client Device" - a network device rather than a "normal" USB attached device.
					*
					*	For new device client connections, we create a fauxPanel to be added to ServerDevices{}.
					*/
					//console.log("device_client connecting ...");
					/*	Start building a device_connect_result message */
					var device_connect_result = {msg_type:"device_connect_result", server_id:ServerID};
					device_connect_result["client_address"] = remoteInfo.address;
					device_connect_result["client_port"] = remoteInfo.port;

					/*	Determine new or returning client */
					const index = client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port);
					if (index < 0) {
						/*	Connection from a new device client */

						/*	Create a faux device object based on supplied connection information */
						const fauxPanel = {};
						if ( msg.hasOwnProperty('device') && msg.hasOwnProperty('vendor_id') && msg.hasOwnProperty('product_id') && msg.hasOwnProperty('unit_id') ) {
							const deviceInfo = {};
							const product = {};

							Object.keys(msg).forEach( (key, index) => {
								if (key == 'msg_type') {
									console.log(`device_connect: Processing ${msg[key]} message`);
								}
								else if (key == 'device') {
									// Not sure about this !!! (using shortnam as name)
									product["name"] = msg[key];
									deviceInfo["product"] = msg[key];
								}
								else if (key == 'vendor_id') {
									product["vendorId"] = msg[key];
									deviceInfo["vendorId"] = msg[key];
								}
								else if (key == 'product_id') {
									product["productId"] = msg[key];
									deviceInfo["productId"] = msg[key];
								}
								else if (key == 'rowCount') {
									product["rowCount"] = msg[key];
								}
								else if (key == 'colCount') {
									product["colCount"] = msg[key];
								}
								else if (key == 'unit_id') {
									fauxPanel["unitId"] = msg[key];
								}
								// else other attributes to be added as required ...
								else {
									console.log(`Unknown device_connect msg key: ${key}`);
								}
								product["interface"] = 0;	// interface: number | null // null means "anything goes", used when interface isn't available
								deviceInfo["interface"] = 0;

								fauxPanel["product"]    = product;
								fauxPanel["deviceInfo"] = deviceInfo;
								fauxPanel["name"]       = `${fauxPanel.deviceInfo.product}`;
								fauxPanel["vendorId"]   = `${fauxPanel.deviceInfo.vendorId}`;
								fauxPanel["uniqueId"]   = `${fauxPanel.deviceInfo.productId}_${fauxPanel.unitId}`;
							});

							Object.defineProperty(fauxPanel, 'info',  {
								get () {
									return {
										name: this.product.name,
										vendorId: this.product.vendorId,
										productId: this.product.productId,
										unitId: this.unitId,
										interface: this.product.interface,
										colCount: this.product.colCount,
										rowCount: this.product.rowCount
									}
								}
							});
							fauxPanel["shortnam"] = msg.device.toString();

							//console.log(`fauxPanel: ${JSON.stringify(fauxPanel)}`);
							server_add_device(fauxPanel);

						} else {
							console.log(`New device_connect msg has insufficient properties to create new device object: ${JSON.stringify(msg, null, 2)}`);
							break;
						}
						//console.log(`fauxPanel: ${JSON.stringify(fauxPanel)}`);

						/*	New device_clients typically don't arrive with a client_name so make one up */
						if (! msg.hasOwnProperty("client_name")) {
							msg["client_name"] = "device_client_" + crypto.randomBytes(8).toString('hex');
						}

						/*	server_add_device() added a duplicate_id, so we can now use the device_quad
							as reference to remove device when client disconnects.
						*/
						const device_quad = fauxPanel.vendorId + "-" + fauxPanel.uniqueId.replace(/_/g, "-") + "-" + fauxPanel.duplicate_id;
						add_client_connection({timestamp:Date.now(), client_name:msg.client_name, remote:remoteInfo, device_quad:device_quad});
						console.log(`Added new device_client: ${msg.client_name}, ${device_quad}`);


					} else {
						/*	Connection from an existing device client - possibly a keep-alive */

						/*	Use existing client_name & device_quad */

						add_client_connection({"timestamp":Date.now(), "client_name":client_connections.list()[index].client_name, "remote":remoteInfo,"device_quad":client_connections.list()[index].device_quad});
					}

					/*	Remainder of device_connect_result */
					device_connect_result["client_name"] = msg.client_name;
					device_connect_result["attached_devices"] = Object.keys(server_devices());
					device_connect_result["version"] = ServerVersion;
					console.log(`Sending: ${JSON.stringify(device_connect_result)}`);
					send_message(JSON.stringify(device_connect_result));

				}	// END fresh scope device_connect
				break;

				case "device_data":
				{	// Fresh scope device_data
					/*	Emit an event based on device_data message from a Device Client */
					//console.log(`Received device_data: ${JSON.stringify(msg)}`);

					/*	Search client_connections to identify device_client device_quad
					*	(thereby access device_client object)
					*/
					const index = client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port);
					if (index < 0) {
						/*	Some device clients (e.g. battery powered, so mostly sleeping)
						*	do not respond to disconnection warnings and are
						*	therefore eventually disconnected by the server.
						*	Such disconnected device clients may still device_data messages
						*	and end up here (since not in client_connections list).
						*
						*	Check here (in LostDeviceClients) whether
						*	this message is from such a disconnected device client.
						*	If so, reconnect the device client and process the device_data message.
						*/
						console.log(`device_data from unconnected client`);
						const lost_keys = Object.keys(LostDeviceClients);
						/*
						lost_keys.forEach( (device_client) => {
							console.log(`${device_client} connection: ${JSON.stringify(LostDeviceClients[device_client].connection)}`);
							console.log(`${device_client} connection: ${LostDeviceClients[device_client].connection.address}`);
							console.log(`${device_client} connection: ${LostDeviceClients[device_client].connection.port}`);
							console.log(`${device_client} connection: ${LostDeviceClients[device_client].connection.transport}`);
						});
						*/
						const lost_index = lost_keys.findIndex( (device_client) => LostDeviceClients[device_client].connection.address === remoteInfo.address
																	&& LostDeviceClients[device_client].connection.port === remoteInfo.port
																	&& LostDeviceClients[device_client].connection.transport === remoteInfo.transport);
						if (lost_index < 0) {
							break;
						}
						fauxPanel = LostDeviceClients[lost_keys[lost_index]].device;
						Object.defineProperty(fauxPanel, 'info',  {
							get () {
								return {
									name: this.product.name,
									vendorId: this.product.vendorId,
									productId: this.product.productId,
									unitId: this.unitId,
									interface: this.product.interface,
									colCount: this.product.colCount,
									rowCount: this.product.rowCount
								}
							}
						});
						server_add_device(fauxPanel);

						/*	server_add_device() added a duplicate_id, so we can now use the device_quad
							as reference to remove device when client disconnects.
						*/
						const device_quad = fauxPanel.vendorId + "-" + fauxPanel.uniqueId.replace(/_/g, "-") + "-" + fauxPanel.duplicate_id;
						add_client_connection({timestamp:Date.now(), client_name:lost_keys[lost_index], remote:remoteInfo, device_quad:device_quad});
						console.log(`Revived lost device_client: ${lost_keys[lost_index]}, ${device_quad}`);

						// Remove from LostDeviceClients
						delete LostDeviceClients[lost_keys[lost_index]];

					}
					const retry_index = client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port);
					if (retry_index < 0 ) {
						console.log(`Something weird going on`);
						return;
					}

					const device_quad = client_connections.list()[retry_index].device_quad;
					const device_client = ServerDevices[device_quad];

					const device = device_client.info.name;
					const vendor_id = device_client.info.vendorId;
					const product_id = device_client.info.productId;
					const unit_id = device_client.info.unitId;
					const duplicate_id = device_client.duplicate_id;

					if (msg.event_type == "button_event") {
						var event_message = {msg_type:"button_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, row:msg.row,col:msg.col, value:msg.value, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "tbar_event") {
						var event_message = {msg_type:"tbar_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, value:msg.value, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "jog_event") {
						var event_message = {msg_type:"jog_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, value:msg.value, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "shuttle_event") {
						var event_message = {msg_type:"shuttle_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, value:msg.value, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "joystick_event") {
						var event_message = {msg_type:"joystick_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, x:msg.x,y:msg.y,z:msg.z,deltaZ:msg.deltaZ, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "rotary_event") {
						var event_message = {msg_type:"rotary_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, value:msg.value, timestamp:msg.timestamp};
					}
					else if (msg.event_type == "trackball_event") {
						var event_message = {msg_type:"trackball_event", server_id:ServerID, device:device,
											vendor_id:vendor_id, product_id:product_id,unit_id:unit_id,duplicate_id:duplicate_id,
											control_id:msg.control_id, x:msg.x,y:msg.y, timestamp:msg.timestamp};
					}
					else {
						console.log(`Unknown event type from device_data message: ${JSON.stringify(msg)}`);
						break;
					}
					send_message(JSON.stringify(event_message));

				}	// END fresh scope device_data
				break;

				case "device_disconnect":
				{	// Fresh scope device_disconnect
					/*	This is a Device Client disconnecting.
					*	As well as removing this connection from client_connections,
					*	we need to remove the device object we created and saved on ServerDevices.
					*/
					//console.log(`device_disconnect`);

					/*	Search client_connections to identify device_client device_quad
					*	(thereby access device_client object)
					*/
					const index = client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port);
					if (index < 0) {
						//	Nothing to do if not connected ???
						console.log(`device_disconnect from unconnected client`);
					} else {
						/* We don't send a disconnect_result message from here - it's done when client is removed from client_connections */
						remove_client_connection(remoteInfo);	//  This also removes device component from ServerDevices
					}

				}	// END fresh scope device_disconnect
				break;

				case "list_attached":
					/*	Generate & send a list of attached devices */
					//console.log(`Processing msg_type:list_attached request`);
					var device_list = [];
					for (const key of Object.keys(ServerDevices)) {
						//console.log(`device: ${key}`);
						//console.log(`device: ${JSON.stringify(ServerDevices[key].info)}`);
						device_list.push(ServerDevices[key].info);
						if (device_list.length > 0 ) {
							device_list[device_list.length-1]["device_quad"] = key;
						}
					}
					send_message(JSON.stringify({msg_type:"list_attached_result",server_id:ServerID, devices:device_list}), remoteInfo);
				break;

				case "all_product_data":
					/*	all_product_data requests a detailed list of product data.
					*	For X-keys products, it's an object made from a dump of the products.ts file.
					*	For other vendors, it's an object contrived at startup to be formatted like the Xk-keys object,
					*	at which time all vendors' product objects are combined to AllProducts.
					*/
					//console.log(`Processing msg_type:all_product_data request`);
					send_message(JSON.stringify({msg_type:"all_product_data_result",server_id:ServerID, data:AllProducts}), remoteInfo);
				break;

				case "list_products":
					//console.log(`Processing msg_type:list_products request`);
					// Optional vendor_id field is: absent => all, -1 => all, vendor_id (or array of vendor_id?)
					const vendor_ids = [];
					if (msg.hasOwnProperty("vendor_id")) {
						if (typeof(msg.vendor_id) == "number") {
							if (msg.vendor_id > 0) {
								vendor_ids.push(msg.vendor_id);
							}
							// else -1 (wildcard) or invalid vendor_id so do nothing.
						} else {
							// already an array of vendor_id
							msg.vendor_id.forEach( (vendor_id) => {
								vendor_ids.push(vendor_id);
							});
						}
					}
					// else nothing to do. Empty vendor_ids[] => all vendors.

					//console.log(`list_products: vendor_ids = ${JSON.stringify(vendor_ids)}`);
					var data = {};
					if (vendor_ids.length > 0 ) {
						Object.keys(ProductSummaries).forEach( (vendor) => {
							console.log(`list_products: vendor = ${vendor}`);
							if (vendor_ids.includes(parseInt(vendor))) {
								console.log(`list_products: adding ${JSON.stringify(ProductSummaries[vendor])}`);
								data[vendor] = ProductSummaries[vendor];
							};
						});
					} else {
						// Empty vendor_ids => send all summaries
						data = ProductSummaries;
					}

					send_message(JSON.stringify({msg_type:"list_products_result",server_id:ServerID, data:data}), remoteInfo);
				break;

				case "product_details":
					//console.log(`Processing msg_type:product_details request`);
					const details = {};
					const names = [];
					if (typeof(msg.name) == "string") {
						// convert to an array
						names.push(msg.name);
					} else {
						// already an array of names
						msg.name.forEach( (name) => {
							names.push(name);
						});
					}
					names.forEach( (name) => {
						details[name] = AllProducts[name];
					});
					send_message(JSON.stringify({msg_type:"product_details_result",server_id:ServerID, data:details}), remoteInfo);
				break;

				case "list_clients":
					var client_list = [];
					client_connections.list().forEach( (client) => {
						client_list.push({"client_address":client.remote.address, "client_port":client.remote.port, "client_name":client.client_name});
					});
					send_message(JSON.stringify({msg_type:"list_clients_result", clients:client_list}), remoteInfo);
				break;

				case "device_layout":
					if (xkeys_layouts_devices.includes(msg.device)) {
						const svg_layout = xkeysLayouts.fetch(msg.device);
						send_message(JSON.stringify({msg_type:"device_layout_result", server_id:ServerID, svg_layout:svg_layout}), remoteInfo);
					}
					/*
					else if ANOTHER_VENDOR_layouts_devices.includes(msg.device)) {
					}
					*/
					else {
						console.log(`device_layout: Unknown device (${msg.device})`);
						// Send error message?
						return;
					}

				break;

				case "reflect":
					//	Quick check that message to reflect is valid
					try {
						JSON.parse(JSON.stringify(msg.message));
					} catch (err) {
						console.log(`reflect: invalid JSON? ${err}`);
						return;
					}
					send_message(JSON.stringify(msg.message));

				break;

				case "command":
					//	First check the command is from a connected client.
					if ((client_connections.list().findIndex(item => item.remote.address === remoteInfo.address && item.remote.port === remoteInfo.port) < 0) && (msg_transport != "mqtt")) {
						console.log(`Command request from unconnected client`);
						send_message(JSON.stringify({msg_type:"error",server_id:ServerID, error_msg:"Client not connected. Try msg_type:'connect'.", error_echo:message}), remoteInfo);
						break;
					}

					//	Process the command request by appropriate hardware module.
					//	Send to all modules, each of which decides if the command is relevant to it.
					config.hw_types.forEach( (hw_type) => {
						try {
							hw[hw_type].process_command(msg);
						}
						catch (err) {
							console.log(`CAUGHT ERROR passing command to hardware: ${err}`);
						}
					})

					msg["pid_list"] = [];
					if (msg.product_id > -1) { msg.pid_list.push(msg.product_id); }
					// If (optional) "duplicate_id" is missing, we assign 0.
					if (! msg.hasOwnProperty("duplicate_id")) { msg["duplicate_id"] = 0; }

					if (msg.command_type == "set_backlight") {
						//console.log("Command: set_backlight");
						msg.name = "setBacklight";
						var params = [];
						// control_id may come in as a single int or an array of ints.
						// we convert to array if necessary for params[0].
						if (typeof(msg.control_id) == "object") {
							params.push(msg.control_id)
						} else {
							var lampids = []; lampids.push(msg.control_id); params.push(lampids);
						}
						// params[1]: colour also doubles as on/off
						if (msg.value == 0) {
							params[1] = "000000";
						} else if (msg.value == 1) {
							params[1] = msg.color;
						} else {
							// Any other value is an error - what to do?
						}
						// params[2]: flash is optional?
						if (msg.hasOwnProperty("flash")) {
						} else {
							// No flash entry => no flashing required
							// Add a nothing value (for use in later reply message)
							msg["flash"] = 0;
							params.push(false);
						}
						msg["params"] = params;

					}

				break;

				default:
					console.log(`request_message_process(): Unhandled msg.request - ${message}`);
			}
		} catch (err) {
			/*	message doesn't comply in some way - either:
			*	- can't JSON.parse it
			*	- some error processing it
			*/
			console.log(`Couldn't process message: ${message.toString()} from ${remoteInfo.address}:${remoteInfo.port}`);
			console.log(err);
			return;
		}

	}
	else if (msg_transport == "mqtt") {
	}
	else if (msg_transport == "donald_duck") {
	}
	else {
		console.log(`Unknown transport: ${msg_transport}`);
	}

	/* Process the incoming message */
	try {
		msg = JSON.parse(message);

	}
	catch (err) {
		console.log(`ERROR Processing incoming message`);
	}

}

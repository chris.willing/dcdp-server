/*      HW_streamdeck.js
*
*       SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*       SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
* 
*       A module adding Elgato Streamdeck devices functionality to DCDP-Server.
*/

const usbDetect = require('usb-detection');
const { listStreamDecks, openStreamDeck } = require('@elgato-stream-deck/node');
const { DEVICE_MODELS, VENDOR_ID } = require('@elgato-stream-deck/core');

const streamDecks = {};
const PRODUCTS = {};
const streamdeck_products = {};

async function addDevice(info) {
	console.log(`addDevice(): Adding ${JSON.stringify(info)}`);
	const start_time = Date.now();	/*	Streamdeck doesn't generate its own timestamp */
	const path = info.path;
	streamDecks[path] = openStreamDeck(path);

	const firmwareVersion =  await streamDecks[path].getFirmwareVersion();
	const serial_number = info.serialNumber;

	/*	Use usbDetect to obtain additional information to add to the streamdeck object
	*/
	usbDetect.find(VENDOR_ID.toString(), function(err, devices) {
		devices.forEach( (device) => {
			if (serial_number == device.serialNumber) {
				//console.log(`Matched streamdeck serial number ${serial_number}`);
				const vendor_id = device.vendorId.toString();
				const product_id = device.productId.toString();
				const unit_id = "0";
				const panel = streamDecks[path];
				panel["vendorId"] = vendor_id;
				panel["uniqueId"] = product_id + "_" + unit_id;

				Object.defineProperty(panel, 'info',  {
					get() {
						return {
							name: panel.device.PRODUCT_NAME,
							vendorId: vendor_id,
							productId: product_id,
							unitId: unit_id,
							interface: 0,
							firmwareVersion: firmwareVersion,
							colCount: panel.device.KEY_COLUMNS,
							rowCount: panel.device.KEY_ROWS,
							emitsTimestamp: true,
							serial_number:serial_number
						}
					}
				});
				panel["shortnam"] = streamdeck_products[panel.info.productId.toString()];
				console.log(`panel: ${JSON.stringify(panel)}`);
				server_add_device(panel);

				var attach_msg = {
					msg_type: "attach_event",
					server_id: server_id(),
					device: panel.info.name,
					vendor_id: panel.info.vendorId,
					product_id: panel.info.productId,
					unit_id: panel.info.unitId,
					duplicate_id: panel.duplicate_id,
					attached_devices: Object.keys(server_devices())
				};
				send_message(JSON.stringify(attach_msg));
			}
		})
	})


	/*	Clear all keys */
	await streamDecks[path].clearPanel();
	// Fill one key in red
	await streamDecks[path].fillKeyColor(0, 255, 0, 0);

	//await streamDecks[path].resetToLogo();


	/*	Device removal */
	streamDecks[path].on('error', (e) => {
		console.log(`Device at ${path} was removed (${e})`);
		// assuming any error means we lost connection
		streamDecks[path].removeAllListeners();


		delete streamDecks[path];
	})



	/*	Event listeners (buttons, tbar, joystick, etc.) go here
	*	Only button events for Elgato Streamdecks.
	*	value = 1 for down, 0 for up
	*/
	streamDecks[path].on('down', (keyIndex) => {
		console.log(`${keyIndex}, ${streamDecks[path].KEY_ROWS}, ${streamDecks[path].KEY_COLUMNS} DOWN`);
		const metadata = {};
		metadata["timestamp"] = Date.now() - start_time;
		const panel = streamDecks[path];
		const rowcol = calcRowCol(keyIndex, panel.KEY_ROWS, panel.KEY_COLUMNS);

		var product_id = panel.info.productId;
		var unit_id = panel.info.unitId;
		metadata["type"] = "down";
		metadata["shortnam"] = streamdeck_products[product_id.toString()];
		metadata["row"] = rowcol[0];
		metadata["col"] = rowcol[1];

		var device_msg_info = {
			"msg_type":"button_event", "server_id":server_id(), "device":panel.info.name,
			"vendor_id":panel.vendorId, "product_id":product_id,"unit_id":unit_id,"duplicate_id":panel.duplicate_id,
			"control_id":keyIndex+1, "row":metadata.row,"col":metadata.col, "value":1,"timestamp":metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})
	streamDecks[path].on('up', (keyIndex) => {
		console.log(`${keyIndex}, ${streamDecks[path].KEY_ROWS}, ${streamDecks[path].KEY_COLUMNS} UP`);
		const metadata = {};
		metadata["timestamp"] = Date.now() - start_time;
		const panel = streamDecks[path];
		const rowcol = calcRowCol(keyIndex, panel.KEY_ROWS, panel.KEY_COLUMNS);

		var product_id = panel.info.productId;
		var unit_id = panel.info.unitId;
		metadata["type"] = "up";
		metadata["shortnam"] = streamdeck_products[product_id.toString()];
		metadata["row"] = rowcol[0];
		metadata["col"] = rowcol[1];

		var device_msg_info = {
			"msg_type":"button_event", "server_id":server_id(), "device":panel.info.name,
			"vendor_id":panel.vendorId, "product_id":product_id,"unit_id":unit_id,"duplicate_id":panel.duplicate_id,
			"control_id":keyIndex+1, "row":metadata.row,"col":metadata.col, "value":0,"timestamp":metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

}

function refresh() {
	const streamdecks = listStreamDecks()
	streamdecks.forEach((device) => {
		if (!streamDecks[device.path]) {
			addDevice(device).catch((e) => console.error('Add failed:', e));
		}
	});
}

calcRowCol = (keyIndex, rows, cols) => {
	var row, col;
	dance:
	for (row=0;row<rows;row++) {
		for (col=0;col<cols;col++) {
			if ((row*cols + col) >= keyIndex) {
				break dance;
			}
		}
	}
	return [row+1,col+1];
}

module.exports = {

	testing () {
		console.log(`Hello from HW_streamdeck`);
	},
	vendorId () {
		return VENDOR_ID.toString();
	},
	start () {
		/*	Generate a local PRODUCTS file (similar format to Xkeys PRODUCTS) */
		DEVICE_MODELS.forEach( (model) => {
			const product_id = model.productId;
			PRODUCTS["SD"+model.id.toUpperCase()] = {"name":"Streamdeck " + model.id.toUpperCase(), "hidDevices":[[product_id,0]]};
		});
		//console.log(`Streamdeck PRODUCTS: ${JSON.stringify(PRODUCTS)}`);

		/*	Now generate reverse lookup of PRODUCTS keys (short name ids), indexed by hidDevice number */
		Object.entries(PRODUCTS).forEach(entry => {
			const [key, value] = entry;
			value.hidDevices.forEach(hidDev => {
				streamdeck_products[hidDev[0]] = key;
			});
		});
		console.log(`streamdeck_products: ${JSON.stringify(streamdeck_products)}`);
		refresh();
	},
	stop () {
		Object.values(streamDecks).forEach( (device) => {
			device.removeAllListeners();
		});
	},
	add (device) {
		console.log(`Adding ${JSON.stringify(device)}`);
		refresh();
	},
	remove (device) {
		console.log(`Removing ${JSON.stringify(device)}`);
	},
	products(products, summary) {
		/*	Add Streamdeck products & summaries in a format matching Xkeys products */
		//console.log(`DEVICE_MODELS = ${JSON.stringify(DEVICE_MODELS)}`);
		const names = [];
		DEVICE_MODELS.forEach( (model) => {
			const product_id = model.productId;
			products["SD"+model.id.toUpperCase()] = {name:"Streamdeck " + model.id.toUpperCase(), vendorId:VENDOR_ID.toString(), hidDevices:[[product_id,0]]};
			const pidlist = [product_id];
			names.push({name:"SD"+model.id.toUpperCase(), pidlist:pidlist});
		});
		summary[VENDOR_ID.toString()] = names;
	},
	process_command (msg) {
		if (msg.vendor_id == VENDOR_ID.toString()) {
			console.log(`streamdeck module processing command`);
			/*	First do stuff that's common to _all_ commands */

			// Generate a list of quads which match command parameters
			const matched_devices = [];
			var expstr = msg.vendor_id.toString() + "-";
			if (msg.product_id == -1) {
				expstr = expstr + "\[0-9\]+-";
			} else {
				expstr = expstr + msg.product_id.toString() + "-";
			}
			if (msg.unit_id == -1) {
				console.log(`blip`);
				expstr = expstr + "\[0-9\]+-";
			} else {
				expstr = expstr + msg.unit_id.toString() + "-";
			}
			if (msg.duplicate_id == -1) {
				console.log(`blip`);
				expstr = expstr + "\[0-9\]+";
			} else {
				expstr = expstr + msg.duplicate_id.toString() + "";
			}
			const re = new RegExp(expstr);
			Object.keys(server_devices()).forEach( (device_quad) => {
				if (device_quad.search(re) > -1) {
					console.log(`Found usable device_quad ${device_quad}`);
					matched_devices.push(device_quad);
				}
			});

			/*	The use of wildcards for product_id, unit_id & duplicate_id
			*	means multiple devices could be affected by any given command
			*	and multiple command_result messages could be required.
			*	To limit the number of command_result messages, we group
			*	together all affected devices which have the same product_id
			*	(since the protocol requires device:SHORTNAME in the result message).
			*	Where multiple unit_id & duplicate_id are involved, they are shown in arrays.
			*/
			const grouped_devices = [];
			grouped_devices_add = (new_quad) => {
				var split_quad = new_quad.split("-");
				if (split_quad.length != 4) {
					//	Something wrong
					console.log(`grouped_devices_add() bad arg: ${new_quad}`);
					return;
				}
				var product_index = grouped_devices.findIndex(item=>item.product_id == split_quad[1]);
				if (product_index > -1) {
					// Already have an entry for this product_id - need to edit it.
					console.log(`editing existing entry for product_id ${split_quad[1]}`);
					grouped_devices[product_index].unit_id.push(parseInt(split_quad[2]));
					grouped_devices[product_index].duplicate_id.push(parseInt(split_quad[3]));
				} else {
					// New entry with this product_id
					console.log(`new entry for product ${split_quad[1]}, ${server_devices()[new_quad].shortnam}`);
					//console.log(`device: ${server_devices()[new_quad].shortnam}`);
					grouped_devices.push({
						msg_type: "command_result", command_type: msg.command_type, server_id: server_id(), device:server_devices()[new_quad].shortnam,
						vendor_id:parseInt(split_quad[0]), product_id:parseInt(split_quad[1]), unit_id:[parseInt(split_quad[2])], duplicate_id:[parseInt(split_quad[3])]
					});
				}
			}	// grouped_devices_add (new_quad)

			/*	Now do stuff specific to each known command
			*/
			if (msg.command_type == "set_unit_id") {
				console.log(`Command: set_unit_id`);

				//	No wildcards for EEPROM commands
				if (msg.vendor_id < 0 || msg.product_id < 0 || msg.unit_id < 0 || msg.duplicate_id < 0) {
					//	Send error message?
					console.log(`Command set_unit_id doesn't accept wildcards`);
					return;
				}
				if (msg.new_unit_id < 0) { // Not aceptable
					//	Send error message?
					console.log(`Command set_unit_id needs a valid new_unit_id - not ${msg.new_unit_id}`);
					return;
				}

				//	Last chance to catch wildcards
				if (matched_devices.length != 1 ) {
					console.log(`Internal error: EEPROM commands should have only a single target - not ${JSON.stringify(matched_devices)}`);
					return;
				}

				const old_name = msg.vendor_id + "-" + msg.product_id + "-" + msg.unit_id + "-" + msg.duplicate_id;
				const new_name = msg.vendor_id + "-" + msg.product_id + "-" + msg.new_unit_id + "-" + msg.duplicate_id;
				//	Ensure new name isn't already being used
				if (server_devices().hasOwnProperty(new_name)) {
					console.log(`Target unit_id (${msg.unit_id}) already exists`);
					return;
				}
				server_devices()[new_name] = server_devices()[old_name];

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad} with ${msg.new_unit_id}`);
					grouped_devices_add(device_quad);
					//	Streamdeck devices don't have this command
					//server_devices()[device_quad].setUnitId(msg.new_unit_id);
					delete server_devices()[old_name];
					//	Reboot device so that the new entity is noticed by the system
					//server_devices()[device_quad].rebootDevice();
				});
				console.log(`New server_devices list: ${Object.keys(server_devices())}`);

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = parseInt(product.unit_id[0]);
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = parseInt(product.duplicate_id[0]);
					}

					product["new_unit_id"] = msg.new_unit_id;

					send_message(JSON.stringify(product));
				});

			} else {
				console.log(`Unhandled command request ${msg.command_type}`);
			}

		}	// msg.vendor_id == VENDOR_ID

	}	// process_command (msg)


}



/*      HW_xkeys.js
*
*       SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*       SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
*
*       A module adding P.I.Engineering Xkeys devices functionality to DCDP-Server.
*/

const { PRODUCTS, XKEYS_VENDOR_ID } = require('@xkeys-lib/core/dist/products');
//const XKeys = require('xkeys');
const { listAllConnectedPanels, setupXkeysPanel } = require('xkeys');
const norm = require('./NormalizeValues.js');

const xkeys_devices = {};

/*	Reverse lookup of PRODUCTS keys (short name ids), indexed by hidDevice number */
let xkeys_products = {};

async function addDevice(xkeysPanel) {
	//console.log(`addDevice(): adding ${JSON.stringify(xkeysPanel)}`);
	const path = xkeysPanel.devicePath;
	xkeys_devices[path] = xkeysPanel;

	xkeysPanel["vendorId"] = XKEYS_VENDOR_ID.toString();
	xkeysPanel["shortnam"] = xkeys_products[xkeysPanel.info.productId.toString()];
	server_add_device(xkeysPanel);
	//console.log(`info: ${xkeysPanel.info.vendorId}`);

	var attach_msg = {
		msg_type: "attach_event",
		server_id: server_id(),
		device: xkeysPanel.info.name,
		vendor_id: xkeysPanel.vendorId,
		product_id: xkeysPanel.info.productId.toString(),
		unit_id: xkeysPanel.info.unitId.toString(),
		duplicate_id: xkeysPanel.duplicate_id,
		attached_devices: Object.keys(server_devices())
	};
	send_message(JSON.stringify(attach_msg));

	/*      Device removal */
	xkeys_devices[path].on('disconnected', () => {
		console.log(`Device at ${path} was removed`)
		// assuming any error means we lost connection
		xkeys_devices[path].removeAllListeners();

		/*	Remove from local record */
		delete xkeys_devices[path];
		console.log(`Remaining devices: ${Object.keys(xkeys_devices)}`);

		/*	Remove from ServerDevices */
		var temp_id = xkeysPanel.vendorId + '-' + xkeysPanel.uniqueId.replace(/_/g, "-") + "-" + xkeysPanel.duplicate_id;
		server_remove_device(temp_id);

		var detach_msg = {
			msg_type: "detach_event",
			server_id: server_id(),
			device: xkeysPanel.info.name,
			vendor_id: xkeysPanel.vendorId,
			product_id: xkeysPanel.info.productId.toString(),
			unit_id: xkeysPanel.info.unitId.toString(),
			duplicate_id: xkeysPanel.duplicate_id,
			attached_devices: Object.keys(server_devices())
		};
		send_message(JSON.stringify(detach_msg));
	})
	xkeys_devices[path].on('down', (keyIndex, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} DOWN`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "down";
		metadata["shortnam"] = xkeys_products[product_id];

		/*	Create an object containing enough info to send message appropriate to any known transport.
		*	Enough info is set in device_msg_info to immediately accomodate a UDP message but
		*	a message for MQTT may be derived from the same information e.g.
		*	  msg_topic = '/xkeys/server/' + button_event + '/' + product_id + '/' + unit_id + '/' + duplicate_id + '/' + control_id
		*	  pload = {"server_id":server_id,"request":"device_event", "data":metadata}
		*	  publish(msg_topic, JSON.stringify(msg_pload))
		*
		*	value = 1 for down, 0 for up
		*/
		var device_msg_info = {
			msg_type:"button_event", server_id:server_id(), device:xkeys_devices[path].info.name,
			vendor_id:xkeysPanel.vendorId, product_id:product_id, unit_id:unit_id, duplicate_id:xkeysPanel.duplicate_id,
			control_id:keyIndex.toString(), row:metadata.row, col:metadata.col, value:1, timestamp:metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));

	})
	xkeys_devices[path].on('up', (keyIndex, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} UP`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "up";
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "button_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: keyIndex.toString(), row:metadata.row, col:metadata.col, value:0, timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

	xkeys_devices[path].on('jog', (index, deltaPos, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} JOG (${index}), delta ${deltaPos}`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "jog";
		metadata["deltaPos"] = norm.normalize(deltaPos, metadata["type"]);
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "jog_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), value: norm.normalize(deltaPos,metadata["type"]), timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})
	xkeys_devices[path].on('shuttle', (index, shuttlePos, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} SHUTTLE (${index}) shuttle ${shuttlePos}`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "shuttle";
		metadata["shuttlePos"] = norm.normalize(shuttlePos, metadata["type"]);
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "shuttle_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), value: norm.normalize(shuttlePos,metadata["type"]), timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

	xkeys_devices[path].on('joystick', (index, position, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} JOYSTICK (${index}) position ${JSON.stringify(position)}`);
		position["x"] = norm.normalize(position["x"], "joyx");
		position["y"] = norm.normalize(position["y"], "joyy");
		position["z"] = norm.normalize(position["z"], "joyz");
		position["deltaZ"] = norm.normalize(position["deltaZ"], "deltaZ");
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "joystick";
		metadata["position"] = position;
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "joystick_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), x:position.x, y:position.y, z:position.z, deltaZ:position.deltaZ, timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

	xkeys_devices[path].on('tbar', (index, position, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} TBAR (${index}) position ${position}`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "tbar";
		metadata["position"] = norm.normalize(position, metadata["type"]);
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "tbar_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), value: metadata.position, timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

	xkeys_devices[path].on('rotary', (index, position, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} ROTARY (${index}) position ${position}`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "rotary";
		metadata["position"] = norm.normalize(position, metadata["type"]);
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "rotary_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), value: metadata.position, timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})

	xkeys_devices[path].on('trackball', (index, position, metadata) => {
		//console.log(`X-keys panel ${xkeys_devices[path].info.name} TRACKBALL (${index}) position ${position}`);
		var product_id = xkeys_devices[path].info.productId.toString();
		var unit_id = xkeys_devices[path].info.unitId.toString();
		if (! metadata.hasOwnProperty("timestamp")) { metadata["timestamp"] = -1; }
		metadata["type"] = "trackball";
		metadata["position"] = position;
		metadata["shortnam"] = xkeys_products[product_id];

		var device_msg_info = {
			msg_type: "trackball_event", server_id: server_id(), device: xkeys_devices[path].info.name,
			vendor_id: xkeysPanel.vendorId, product_id: product_id, unit_id: unit_id, duplicate_id: xkeysPanel.duplicate_id,
			control_id: index.toString(), x:position.x, y:position.y, timestamp: metadata.timestamp
		};
		send_message(JSON.stringify(device_msg_info));
	})
}


function refresh() {
	const xkeys = listAllConnectedPanels();
	xkeys.forEach((device) => {
		//console.log(`setting up ${JSON.stringify(device)}`);
		setupXkeysPanel(device)
			.then( (xkeysPanel) => {
				//console.log(`setting up at path ${xkeysPanel.devicePath}`);
				if (!xkeys_devices[xkeysPanel.devicePath]) {
					//console.log(`Adding NEW device: ${xkeysPanel.devicePath}`);
					addDevice(xkeysPanel).catch((e) => console.error('Add failed:', e));
				}
			})
			.catch(console.log);
		})
}


module.exports = {

	testing () {
		console.log(`Hello from HW_xkeys`);
	},
	vendorId () {
		return XKEYS_VENDOR_ID.toString();
	},
	start () {
		console.log(`ServerID = ${server_id()}`);

		/*	Populate xkeys_products */
		Object.entries(PRODUCTS).forEach(entry => {
			const [key, value] = entry;
			value.hidDevices.forEach(hidDev => {
				xkeys_products[hidDev[0]] = key;
			});
		});
		refresh();
	},
	stop () {
		Object.values(xkeys_devices).forEach( (device) => {
			device.removeAllListeners();
		});
	},
	add (device) {
		console.log(`Adding ${JSON.stringify(device)}`);
		refresh();
	},
	remove (device) {
		console.log(`Removing device ${device.deviceName}`);
		//refresh();
	},
	products (products, summary) {
		//	Add products & summaries
		const names = [];
		Object.keys(PRODUCTS).forEach( (name) => {
			products[name] = PRODUCTS[name];

			// Add vendorId so we can filter products lists by it
			products[name]["vendorId"] = XKEYS_VENDOR_ID.toString();

			//	For summary, we want {name:shortname, pidlist:[]}
			const pidlist = [];
			PRODUCTS[name].hidDevices.forEach( (entry)=>{
				//console.log(`products: ${entry[0]}`);
				pidlist.push(entry[0]);
			});
			names.push({name:name, pidlist:pidlist});
		});
		summary[XKEYS_VENDOR_ID.toString()] = names;
	},
	process_command (msg) {
		if (msg.vendor_id == XKEYS_VENDOR_ID.toString()) {
			console.log(`xkeys module processing command ${JSON.stringify(msg)}`);
			/*	First do stuff that's common to _all_ commands */

			// Generate a list of quads which match command parameters
			const matched_devices = [];
			var expstr = msg.vendor_id.toString() + "-";
			if (msg.product_id == "-1") {
				expstr = expstr + "\[0-9\]+-";
			} else {
				expstr = expstr + msg.product_id.toString() + "-";
			}
			if (msg.unit_id == "-1") {
				expstr = expstr + "\[0-9\]+-";
			} else {
				expstr = expstr + msg.unit_id + "-";
			}
			if (msg.duplicate_id == "-1") {
				expstr = expstr + "\[0-9\]+";
			} else {
				expstr = expstr + msg.duplicate_id.toString() + "";
			}
			const re = new RegExp(expstr);
			Object.keys(server_devices()).forEach( (device_quad) => {
				if (device_quad.search(re) > -1) {
					console.log(`Found usable device_quad ${device_quad}`);
					matched_devices.push(device_quad);
				}
			});

			/*  The use of wildcards for product_id, unit_id & duplicate_id
			*   means multiple devices could be affected by any given command
			*   and multiple command_result messages could be required.
			*   To limit the number of command_result messages, we group
			*   together all affected devices which have the same product_id
			*   (since the protocol requires device:SHORTNAME in the result message).
			*
			*   Where multiple unit_id & duplicate_id are involved,
			*   they are shown as comma separated strings if ID numbers.
			*/
			const grouped_devices = [];
			grouped_devices_add = (new_quad) => {
				var split_quad = new_quad.split("-");
				if (split_quad.length != 4) {
					//	Something wrong
					console.log(`grouped_devices_add() bad arg: ${new_quad}`);
					return;
				}
				var product_index = grouped_devices.findIndex(item=>item.product_id == split_quad[1]);
				if (product_index > -1) {
					// Already have an entry for this product_id - need to edit it.
					//console.log(`editing existing entry for product_id ${split_quad[1]}`);
					grouped_devices[product_index].unit_id += `,${split_quad[2]}`;
					grouped_devices[product_index].duplicate_id += `,${split_quad[3]}`;
				} else {
					// New entry with this product_id
					//console.log(`new entry for product ${split_quad[1]}, ${server_devices()[new_quad].shortnam}`);
					//console.log(`device: ${server_devices()[new_quad].shortnam}`);
					grouped_devices.push({
						msg_type: "command_result", command_type: msg.command_type, server_id: server_id(), device:server_devices()[new_quad].shortnam,
						vendor_id:split_quad[0], product_id:split_quad[1], unit_id:split_quad[2], duplicate_id:split_quad[3]
					});
				}
			}

			/*	Now do stuff specific to each known command
			*/
			if (msg.command_type == "set_indicator_led") {
				console.log(`Command: set_indicator_led`);
				// control_id may come in as a single int (as string), or string of comma separated ints, or (deprecated) an array of ints.
				// we convert to array
				var ledids = [];
				if (typeof(msg.control_id) == "object") {
					// Deprecated - we now prefer multiple keys to be sent as string of comma separated numbers
					msg.control_id.forEach( (id) => {
						if (id) ledids.push(id);
					});
				} else {
					if (msg.control_id.length > 0) {
						ledids = msg.control_id.split(',');
					}
				}

				// Run the command
				//console.log(`matched_devices = ${JSON.stringify(matched_devices)}`);
				matched_devices.forEach( (device_quad) => {
					//console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					ledids.forEach( (led) => {
						server_devices()[device_quad].setIndicatorLED(parseInt(led), 1==msg.value, 1==msg.flash);
					});
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["control_id"] = ledids.join();
					product["value"] = msg.value;
					product["flash"] = msg.flash;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "set_flash_rate") {
				console.log(`Command: set_flash_rate`);
				//	Check msg.value
				msg.value = parseInt(msg.value);
				if (isNaN(msg.value)) { msg.value = 40 }

				if (msg.value > 255) { msg.value = 255; }
				if (msg.value < 0) { msg.value = 0; }

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].setFrequency(msg.value);
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["value"] = msg.value;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "set_all_backlights") {
				console.log(`Command: set_all_backlights`);
				//	Current interpretation of value & color fields:
				//	value == 0 ? set color 000000
				//	value == 1 ? use color as is
				if (msg.value == 0) { msg.color = "000000"; }

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].setAllBacklights(msg.color);
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["value"] = msg.value;
					product["color"] = msg.color;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "set_backlight_intensity") {
				console.log(`Command: set_backlight_intensity`);
				//	TODO: decide how (or whether) to include msg.color

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].setBacklightIntensity(msg.value[0], msg.value[1]);
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["value"] = msg.value;
					//product["color"] = msg.color;

					send_message(JSON.stringify(product));

				});

			}
			else if (msg.command_type == "write_data") {
				console.log(`Command: write_data`);
				//	Copy byte_array
				const data_incoming = [];
				msg.byte_array.forEach( (byte) => {
					data_incoming.push(byte);
				});

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad} with ${msg.byte_array}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].writeData(msg.byte_array);
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					//product["byte_array"] = msg.byte_array;
					product["byte_array"] = data_incoming;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "write_lcd_display") {
				console.log(`Command: write_lcd_display`);

				//	Convert msg.line & msg.text into array entries
				var lines = []; var texts = [];
				if (typeof(msg.line) == "object") {
					msg.line.forEach( (id) => {
						lines.push(id);
					});
				} else {
					lines.push(msg.line);
				}
				if (typeof(msg.text) == "object") {
					msg.text.forEach( (id) => {
						texts.push(id);
					});
				} else {
					texts.push(msg.text);
				}
				if ((lines.length != texts.length) || (lines.length > 2)) {
					console.log(`Wrong input for write_lcd_display`);
					return;
				}
				var product_index = grouped_devices.findIndex(item=>item.product_id == split_quad[1]);
				const oob = lines.findIndex(item=>(item  < 1 || item >2));
				if (oob > -1 ) {
					console.log(`line number ${lines[oob]} out of bounds`);
					return;
				}

				//	In case backlight is optional (must be 0 or 1).
				if( !msg.hasOwnProperty("backlight") || (![0,1].includes(msg.backlight)) ) {
					msg["backlight"] = 1;
				}

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					for (var i=0;i<lines.length;i++) {
						//if (texts[i].length > 0) {
							//console.log(`lcd writing line ${parseInt(lines[i])} |${texts[i]}|`);
							server_devices()[device_quad].writeLcdDisplay(parseInt(lines[i]), texts[i], 1==msg.backlight);
						//}
					}
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					if (lines.length == 1) {
						product["line"] = parseInt(lines);
					} else {
						product["line"] = lines;
					}
					if (texts.length == 1) {
						product["text"] = texts[0];
					} else {
						product["text"] = texts;
					}
					product["backlight"] = msg.backlight;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "set_backlight") {
				console.log(`Command: set_backlight`);
				// control_id may come in as a single int (as string), or string of comma separated ints, or (deprecated) an array of ints.
				// we convert to array
				var lampids = [];
				if (typeof(msg.control_id) == "object") {
					// Deprecated - we now prefer multiple keys to be sent as string of comma separated numbers
					msg.control_id.forEach( (id) => {
						if (id) lampids.push(id);
					});
				} else {
					if (msg.control_id.length > 0) {
						lampids = msg.control_id.split(',');
					}
				}

				// Run the command
				//	TODO - check the color value is OK?
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					lampids.forEach( (lamp) => {
						//console.log(` running setBacklight ${parseInt(lamp)}, ${msg.color}, ${1==msg.flash}`);
						server_devices()[device_quad].setBacklight(parseInt(lamp), msg.color, 1==msg.flash);
					});
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["control_id"] = lampids.join();
					product["color"] = msg.color;
					product["flash"] = msg.flash;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "set_unit_id") {
				console.log(`Command: set_unit_id`);

				//	No wildcards for EEPROM commands
				if (parseInt(msg.vendor_id) < 0 || parseInt(msg.product_id) < 0 || parseInt(msg.unit_id) < 0 || parseInt(msg.duplicate_id) < 0) {
					//	Send error message?
					console.log(`Command set_unit_id doesn't accept wildcards`);
					return;
				}
				if (parseInt(msg.new_unit_id) < 0) { // Not aceptable
					//	Send error message?
					console.log(`Command set_unit_id needs a valid new_unit_id - not ${msg.new_unit_id}`);
					return;
				}

				//	Last chance to catch wildcards
				if (matched_devices.length != 1 ) {
					console.log(`Internal error: EEPROM commands should have only a single target - not ${JSON.stringify(matched_devices)}`);
					return;
				}

				const old_name = msg.vendor_id + "-" + msg.product_id + "-" + msg.unit_id + "-" + msg.duplicate_id;
				const new_name = msg.vendor_id + "-" + msg.product_id + "-" + msg.new_unit_id + "-" + msg.duplicate_id;
				//	Ensure new name isn't already being used
				if (server_devices().hasOwnProperty(new_name)) {
					console.log(`Target unit_id (${msg.unit_id}) already exists`);
					return;
				}
				server_devices()[new_name] = server_devices()[old_name];

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad} with ${msg.new_unit_id}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].setUnitId(msg.new_unit_id);
					delete server_devices()[old_name];
					//server_devices()[device_quad].rebootDevice();
				});
				console.log(`New server_devices list: ${Object.keys(server_devices())}`);

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					product["new_unit_id"] = msg.new_unit_id;

					send_message(JSON.stringify(product));
				});

			}
			else if (msg.command_type == "save_backlight") {
				console.log(`Command: save_backlight`);

				//	No wildcards for EEPROM commands
				if (parseInt(msg.vendor_id) < 0 || parseInt(msg.product_id) < 0 || parseInt(msg.unit_id) < 0 || parseInt(msg.duplicate_id) < 0) {
					//	Send error message?
					console.log(`Command save_backlight doesn't accept wildcards`);
					return;
				}

				//	Last chance to catch wildcards
				if (matched_devices.length != 1 ) {
					console.log(`Internal error: EEPROM commands should have only a single target - not ${JSON.stringify(matched_devices)}`);
					return;
				}

				// Run the command
				matched_devices.forEach( (device_quad) => {
					console.log(`commanding ${device_quad}`);
					grouped_devices_add(device_quad);
					server_devices()[device_quad].saveBackLights();
				});

				/*	command_result messages.
				*/
				//	Convert single value arrays to integer and add remaining command_result message fields
				grouped_devices.forEach( (product) => {
					if (product.unit_id.length == 1) {
						product.unit_id = product.unit_id[0];
					}
					if (product.duplicate_id.length == 1) {
						product.duplicate_id = product.duplicate_id[0];
					}

					send_message(JSON.stringify(product));
				});

			} else {
				console.log(`Unhandled command request ${msg.command_type}`);
			}
		}
	}

}

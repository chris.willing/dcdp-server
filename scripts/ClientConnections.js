"use strict";

const	CLIENT_TTL_DEFAULT = 100000;
const	CLIENT_WARNING_TTL_DEFAULT = 20000;
const	CLIENT_TTL_WARNINGS_DEFAULT = 2;

/*	This module calls two out-of-module functions
	which are expected to be provided by the calling environment.

			show_client_connections();
			remove_client_connection(client.remote);
*/

module.exports = class ClientConnections {
	constructor (...client_ttls) {
		if (client_ttls.length == 0) {client_ttls = [{}];};
		console.log(`new ClientConnections: ${JSON.stringify(client_ttls)}`);
		console.log(`new ClientConnections client_ttl: ${client_ttls[0].client_ttl}`);
		this.connected_clients = [];
		this.client_ttl = client_ttls[0].client_ttl || CLIENT_TTL_DEFAULT;
		this.client_warning_ttl = client_ttls[0].client_warning_ttl || CLIENT_WARNING_TTL_DEFAULT;
		this.ttl_warnings = client_ttls[0].ttl_warnings || CLIENT_TTL_WARNINGS_DEFAULT;
		console.log(`ClientConnections instance with client_ttl = ${this.client_ttl}, client_warning_ttl = ${this.client_warning_ttl}, ttl_warnings = ${this.ttl_warnings}`);
	}

	add (client) {
		console.log(`ADDING a connection: ${client.client_name} from ${client.remote.address}:${client.remote.port}`);

		const index = this.connected_clients.findIndex(item => item.remote.address === client.remote.address && item.remote.port === client.remote.port);
		if (index < 0) {
			//	New client connection. Add a timeout object.
			client["ttl_timer"] = setTimeout(this.send_ttl_warning.bind(this), this.client_ttl, client);
			client["warnings"] = 0;
			this.connected_clients.push(client);
		} else {
			//	Existing connection to either keep alive or change name
			clearTimeout(this.connected_clients[index].ttl_timer);
			this.connected_clients[index].warnings = 0;
			this.connected_clients["ttl_timer"] = setTimeout(this.send_ttl_warning.bind(this), this.client_ttl, this.connected_clients[index]);

			//	In case of a name change
			this.connected_clients[index].client_name = client.client_name;

		}
		//show_client_connections();
	}

	/*	remove (connection)
	*
	*	connection is a remoteInfo object
	*	(not the full client object), so
	*	find the client with the given connection
	*	information and remove it from connected_clients.
	*
	*	The device component of a device client must be handled elsewhere.
	*	We only deal with the client component here!
	*/
	remove (connection) {
		console.log(`REMOVING client connection at ${connection.address}`);

		const index = this.connected_clients.findIndex(item => item.remote.address === connection.address && item.remote.port === connection.port);
		if (index < 0 ) {
			//	No match found
			console.log(`remove(): unknown connection (already disposed of?)`);
		} else {
			console.log(`disconnecting ${this.connected_clients[index].client_name}`);
			//	First remove ttl timer
			clearTimeout(this.connected_clients[index].ttl_timer);

			//	Prepare a disconnect_result message
			let disconnect_result;
			if (this.connected_clients[index].hasOwnProperty("device_quad")) {
				disconnect_result = {msg_type:"device_disconnect_result", server_id:server_id(), device:this.connected_clients[index].device};
			} else {
				disconnect_result = {msg_type:"disconnect_result",server_id:server_id()};
			}
			disconnect_result["client_address"] = this.connected_clients[index].remote.address;
			disconnect_result["client_port"] = this.connected_clients[index].remote.port;
			disconnect_result["client_name"] = this.connected_clients[index].client_name;
			this.connected_clients.splice(index, 1);
			//	To disconnecting client
			send_message(JSON.stringify(disconnect_result), connection);
			//	To remaining clients
			send_message(JSON.stringify(disconnect_result));

			//	Other cleanups
			if (connection.hasOwnProperty("sock")) {
				try {
					connection.sock.end();
				} catch (err) {
				}
			}
		}
	}

	/*	reset_timer(connection)
	*
	*	Restart timer sequence for this connection.
	*	Typically used whenever message is received from this connection.
	*/
	reset_timer (connection) {
		const index = this.connected_clients.findIndex(item => item.remote.address === connection.address && item.remote.port === connection.port);
		if (index < 0 ) {
			//	No match found
			return;
		}

		//console.log(`Reset ttl timer for connection ${JSON.stringify(connection)}`);
		const client = this.connected_clients[index];
		clearTimeout(client.ttl_timer);
		client.ttl_timer = setTimeout(this.send_ttl_warning.bind(this), this.client_ttl, client);
		client.warnings = 0;
	}

	/*	list ()
	*
	*	Return array of connected clients
	*/
	list () {
		return this.connected_clients;
	}

	/*	name (connection)
	*
	*	Return the client_name of a connected client
	*/
	name (connection) {
		const index = this.connected_clients.findIndex(item => item.remote.address === connection.address && item.remote.port === connection.port);
		if (index < 0 ) {
			//	No match found
			console.log(`name(): unknown connection`);
			return undefined;
		} else {
			console.log(`name(): ${this.connected_clients[index].client_name} is disconnecting`);
			return this.connected_clients[index].client_name;
		}
	}

	/*	send_ttl_warning(client)
	*	
	*	If the client has not communicated within a certain time,
	*	send it two warnings that it should communicate somehow
	*	(perhaps with another connect message)
	*/
	send_ttl_warning (client) {
		//console.log(`send_ttl_warning()`);

		//	Check we have a real client
		const index = this.connected_clients.findIndex(item => item.remote.address === client.remote.address && item.remote.port === client.remote.port);
		if (index < 0) {
			//console.log(`${client.client_name} shouldn't exist`);
			return;
		}


		//console.log(`warnings = ${this.connected_clients[index].warnings}`);
		console.log(`warnings = ${client.warnings}`);
		//if (this.connected_clients[index].warnings < this.ttl_warnings) {
		if (client.warnings < this.ttl_warnings) {
			//	Send another warning
			const disconnect_warning = {msg_type:"disconnect_warning",server_id:server_id()};
			disconnect_warning["client_address"] = client.remote.address;
			disconnect_warning["client_port"] = client.remote.port;
			disconnect_warning["client_name"] = client.client_name;
			send_message(JSON.stringify(disconnect_warning), client.remote);

			//	Check again later
			client.warnings += 1;
			client.ttl_timer = setTimeout(this.send_ttl_warning.bind(this), this.client_warning_ttl, client);
			//this.connected_clients[index].warnings += 1;
			//this.connected_clients["ttl_timer"] = setTimeout(this.send_ttl_warning.bind(this), this.client_warning_ttl, this.connected_clients[index]);

			//console.log(`send_ttl_warning() ${this.connected_clients[index].warnings}`);
			console.log(`send_ttl_warning() ${client.warnings}`);
			show_client_connections();

		} else {
			//	Enough warnings - disconnect the client
			//console.log(`send_ttl_warning() time to die (warnings = ${this.connected_clients[index].warnings} for ${this.connected_clients[index].client_name})`);
			console.log(`send_ttl_warning() time to die (warnings = ${client.warnings} for ${this.connected_clients[index].client_name})`);

			//this.remove(client.remote);
			/*	We call non-module remove_client_connection()
			*	which handles the device component of a device client
			*	and which then calls this.remove()
			*/
			remove_client_connection(client.remote);
		}
	}

};



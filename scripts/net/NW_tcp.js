/*      NW_tcp.js
*
*       SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*       SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
* 
*       A plugin to add TCP messaging functionality to dcdp-server.
*/

const os = require('os');
const mdns = require('mdns');

const server_address  = "0.0.0.0";
const server_port     = 47795;

const net = require('net');

/*	Keep track of client connection sockets
*	for sending messages.
*/
const client_connections = [];

let tcp_server;
var mdns_needs_restart = true;


module.exports = {

	testing () {
		console.log(`Hello from NW_tcp using address ${server_address}, port ${server_port}, ServerID = ${server_id()}`);
	},

	start () {
		console.log(`Starting TCP service at ${server_address}, port ${server_port}`);
		const tcp_host = server_address;
		const tcp_port = server_port;
		tcp_server = net.createServer(onClientConnected);
		tcp_server.listen(tcp_port, tcp_host, function() {
			console.log(`TCP server listening at ${JSON.stringify(tcp_server.address())}`);
		});


		function onClientConnected(sock) {
			var remoteInfo = {address:sock.remoteAddress, port:sock.remotePort, transport:"tcp", sock:sock};
			//console.log(`onClientConnected() - new client connected: ${JSON.stringify(remoteInfo)}`);

			sock.on('error', (err) => {
				console.log(err.stack);
				// Remember to remove from client list
				try {
					sock.close();
				} catch (err) {
				}
			});
			sock.on('close', () => {
				console.log(`connection from ${remoteInfo.address} closed`);
				// Remember to remove from client list
				remove_client_connection(remoteInfo);
			});
			sock.on('data', (data) => {
				console.log(`Client message \"${data}\" from ${remoteInfo.address}:${remoteInfo.port}`);
				request_message_process(data, remoteInfo);

			});
		}

	},

	send (message, remoteInfo) {
		//console.log(`NW_tcp send(): ${message} to ${JSON.stringify(remoteInfo)}`);
		//	Check message is valid JSON
		try {
			var msg = JSON.parse(message);
		}
		catch (err) {
			console.log(`send(): Exception parsing message. ${err}`);
			return;
		}

		try {
			//console.log(`send() message length = ${message.length}`);
			remoteInfo.sock.write(message + "\r\n");
		} catch (err) {
			console.log(`NW_tcp write() ERROR sending: ${err}`);
		}
	},

	check_mdns () {
		//console.log(`check_mdns(): mdns_needs_restart = ${mdns_needs_restart}`);
		if (mdns_needs_restart) {
			console.log(`check_mdns() attempting to start mdns advertisement`);
			const ServerID = server_id();
			find_local_addresses().forEach( (address) => {
				try {
					console.log(`Advertise local address: ${address}`);
					const ad = new mdns.Advertisement('_dcdp._tcp', server_port, {
						networkInterface: address
						, txtRecord: {oaddr: address, oid: ServerID, oport: server_port}
					});
					ad.start();
					mdns_needs_restart = false;
					ad.on('error', exception => {
						console.log(`have mdns Advertisement ERROR: ${exception.toString()}`);
						mdns_needs_restart = true;
					});
				} catch (err) {
					console.log(`ERROR!!! mdns problem - has avahidaemon stopped? ERROR is: ${err}`);
					mdns_needs_restart = true;
				}
			});
		}
	},

	/*	services()
	*
	*	Return an array of service {port:...,address:...} provided by this transport
	*/
	services () {
		const info = [];
		find_local_addresses().forEach( (address) => {
			info.push({"port":server_port, "ifaddress":address});
		});
		return info;
	}

};

function find_local_addresses () {
	//console.log(`find_local_addresses()`);
	var addresses = [];
	const ifaces = os.networkInterfaces();
	Object.keys(ifaces).forEach( (iface) => {
		//console.log(`${JSON.stringify(ifaces)}`);

		/*	Don't list loopback interface.
		*	"lo" for linux
		*	"lo0" for macOS (possibly lo1, lo2, ...?)
		*	"Loopback Pseudo-Interface ..." for Windows
		*/
		if (iface == "lo" || iface.match(/lo[0-9]/) || iface.match(/Loop/)) { return; }

		for (var i=0;i<ifaces[iface].length;i++) {
			if (ifaces[iface][i].family == "IPv4") {
				//console.log(`${JSON.stringify(ifaces[iface][i])}`);
				//console.log(`${JSON.stringify(ifaces[iface][i].address)}`);
				addresses.push(ifaces[iface][i].address);
			}
		}
	});
	return addresses;
}

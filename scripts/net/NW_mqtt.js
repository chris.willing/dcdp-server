/*      NW_mqtt.js
*
*       SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*       SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
* 
*       A plugin to add MQTT messaging functionality to dcdp-server.
*/

const os = require('os');
const mqtt = require('mqtt');
const mdns = require('mdns');

const server_address  = "0.0.0.0";
const server_port     = 1883;

const net = require('net');

/*	Keep track of client connection sockets
*	for sending messages.
*/
const client_connections = [];

const qos = 2;
let mqtt_server;
var mdns_needs_restart = true;

const connectUrl = "mqtt://localhost";
let	client;


module.exports = {

	testing () {
		console.log(`Hello from NW_mqtt using address ${server_address}, port ${server_port}, ServerID = ${server_id()}`);
	},

	start () {
		console.log(`Starting DCDP service for MQTT at ${server_address}, port ${server_port}`);
		client = mqtt.connect(connectUrl);

		client.on('reconnect', (error) => {
			//console.log('reconnecting:', error);
		});

		client.on('error', (error) => {
			//console.log('Connection failed:', error);
		});

		client.on('connect', () => {
			//console.log('connected to mqtt server');
			client.publish('/dcdp/server', JSON.stringify({server_id:server_id(), msg_type:"hello",data:"Hello from DCDP device server"}),{qos:qos,retain:false});
			client.subscribe({'/dcdp/node/#':{qos:qos}}, function (err) {
				if (!err) {
					console.log(`dcdp-server subscribed to MQTT server OK`);
				} else {
					// Any point in going on?
					console.log('MQTT subscription failed: ' + err);
					process.exit(1);
				}
			});
		});

		client.on('message', (topic, message) => {
			//console.log(`NW_mqtt received message with topic: ${topic}, message: ${message}`);
			try {
				var msg = JSON.parse(message);
				if (msg.hasOwnProperty("payload")) {
					//console.log(`NW_mqtt received msg has a payload field`); // Should probably remove it
					request_message_process(JSON.stringify(msg.payload), {topic: topic, transport:"mqtt"});
				} else {
					//console.log(`NW_mqtt received msg has NO payload field`);
					request_message_process(JSON.stringify(msg), {topic: topic, transport:"mqtt"});
				}
			}
			catch (err) {
				console.log(`NW_mqtt couldn't parse incoming message ${message}`);
			}
		});
	},

	send (message) {
		//console.log(`NW_mqtt send(): ${message}`);
		//	Check message is valid JSON
		try {
			var msg = JSON.parse(message);
		}
		catch (err) {
			console.log(`send(): Exception parsing message. ${err}`);
			return;
		}

		const msg_topic = "/dcdp/server";
		try {
			//console.log(`NW_mqtt send() message topic = ${msg_topic}`);
			//console.log(`NW_mqtt send() message type = ${typeof(message)}`);
			client.publish(msg_topic, message, {qos:qos,retain:false});
		} catch (err) {
			console.log(`NW_mqtt write() ERROR sending: ${err}`);
		}
	},

	check_mdns () {
		//console.log(`check_mdns(): mdns_needs_restart = ${mdns_needs_restart}`);
		if (mdns_needs_restart) {
			console.log(`check_mdns() attempting to start mdns advertisement`);
			const ServerID = server_id();
			find_local_addresses().forEach( (address) => {
				try {
					console.log(`Advertise local address: ${address}`);
					const ad = new mdns.Advertisement('_mqtt._tcp', server_port, {
						name: 'MQTT service at ' + address
						, networkInterface: address
						, txtRecord: {oaddr: address, oid: ServerID, oport: server_port}
					});
					ad.start();
					mdns_needs_restart = false;
					ad.on('error', exception => {
						console.log(`have mdns Advertisement ERROR: ${exception.toString()}`);
						mdns_needs_restart = true;
					});
				} catch (err) {
					console.log(`ERROR!!! mdns problem - has avahidaemon stopped? ERROR is: ${err}`);
					mdns_needs_restart = true;
				}
			});
		}
	},

	/*	services()
	*
	*	Return an array of service {port:...,address:...} provided by this transport
	*/
	services () {
		const info = [];
		find_local_addresses().forEach( (address) => {
			info.push({"port":server_port, "ifaddress":address});
		});
		return info;
	}

};

function find_local_addresses () {
	//console.log(`find_local_addresses()`);
	var addresses = [];
	const ifaces = os.networkInterfaces();
	Object.keys(ifaces).forEach( (iface) => {
		//console.log(`${JSON.stringify(ifaces)}`);

		/*	Don't list loopback interface.
		*	"lo" for linux
		*	"lo0" for macOS (possibly lo1, lo2, ...?)
		*	"Loopback Pseudo-Interface ..." for Windows
		*/
		if (iface == "lo" || iface.match(/lo[0-9]/) || iface.match(/Loop/)) { return; }

		for (var i=0;i<ifaces[iface].length;i++) {
			if (ifaces[iface][i].family == "IPv4") {
				//console.log(`${JSON.stringify(ifaces[iface][i])}`);
				//console.log(`${JSON.stringify(ifaces[iface][i].address)}`);
				addresses.push(ifaces[iface][i].address);
			}
		}
	});
	return addresses;
}

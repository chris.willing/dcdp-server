/*      NW_udp.js
*
*       SPDX-License-Identifier: MIT OR LGPL-2.0-or-later
*       SPDX-FileCopyrightText: 2022 Christoph Willing <chris.willing@linux.com>
* 
*       A plugin to add UDP messaging functionality to dcdp-server.
*/

const os = require('os');
const mdns = require('mdns');

const server_address  = "0.0.0.0";
const server_port     = 48895;

const dgram = require('dgram');
const udp_clients = [];

let udp_server;
var mdns_needs_restart = true;


module.exports = {

	testing () {
		console.log(`Hello from NW_udp using address ${server_address}, port ${server_port}, ServerID = ${server_id()}`);
	},

	start () {
		console.log(`Starting UDP service at ${server_address}, port ${server_port}`);
		udp_server = dgram.createSocket('udp4');
		const udp_host = server_address;
		const udp_port = server_port;
		udp_server.bind(udp_port, udp_host);

		udp_server.on('error', (err) => {
			console.log(err.stack);
			udp_server.close();
		});

		udp_server.on('listening', () => {
			const address = udp_server.address();
			console.log(`server listening ${address.address}:${address.port}`);
		});

		udp_server.on('message', (message, rinfo) => {
			console.log(`Client message \"${message}\" from ${rinfo.address}:${rinfo.port}`);
			request_message_process(message, {address:rinfo.address, port:rinfo.port, transport:"udp"});
		});
	},

	send (message, remoteInfo) {
		//console.log(`NW_udp send(): ${message} to ${JSON.stringify(remoteInfo)}`);
		//	Check message is valid JSON
		try {
			var msg = JSON.parse(message);
		}
		catch (err) {
			console.log(`NW_udp send(): Exception parsing message. ${err}`);
			return;
		}

		// Send only to specified address,port
		try {
			//console.log(`send() message length = ${message.length}`);
			udp_server.send(message, remoteInfo.port, remoteInfo.address);
		} catch (err) {
			console.log(`NW_udp send() ERROR sending: ${err}`);
		}
	},

	check_mdns () {
		//console.log(`check_mdns(): mdns_needs_restart = ${mdns_needs_restart}`);
		if (mdns_needs_restart) {
			console.log(`check_mdns() attempting to start mdns advertisement`);
			const ServerID = server_id();
			find_local_addresses().forEach( (address) => {
				try {
					console.log(`Advertise local address: ${address}`);
					const ad = new mdns.Advertisement('_dcdp._udp', server_port, {
						name: 'DCD Protocol server at ' + address
						, networkInterface: address
						, txtRecord: {oaddr: address, oid: ServerID, oport: server_port}
					});
					ad.start();
					mdns_needs_restart = false;
					ad.on('error', exception => {
						console.log(`have mdns Advertisement ERROR: ${exception.toString()}`);
						mdns_needs_restart = true;
					});
				} catch (err) {
					console.log(`ERROR!!! mdns problem - has avahidaemon stopped? ERROR is: ${err}`);
					mdns_needs_restart = true;
				}
			});
		}
	},

	/*	services()
	*
	*	Return an array of service {port:...,address:...} provided by this transport
	*/
	services () {
		const info = [];
		find_local_addresses().forEach( (address) => {
			info.push({"port":server_port, "ifaddress":address});
		});
		return info;
	}

};

function find_local_addresses () {
	//console.log(`find_local_addresses()`);
	var addresses = [];
	const ifaces = os.networkInterfaces();
	Object.keys(ifaces).forEach( (iface) => {
		//console.log(`${JSON.stringify(ifaces)}`);

		/*	Don't list loopback interface.
		*	"lo" for linux
		*	"lo0" for macOS (possibly lo1, lo2, ...?)
		*	"Loopback Pseudo-Interface ..." for Windows
		*/
		if (iface == "lo" || iface.match(/lo[0-9]/) || iface.match(/Loop/)) { return; }

		for (var i=0;i<ifaces[iface].length;i++) {
			if (ifaces[iface][i].family == "IPv4") {
				//console.log(`${JSON.stringify(ifaces[iface][i])}`);
				//console.log(`${JSON.stringify(ifaces[iface][i].address)}`);
				addresses.push(ifaces[iface][i].address);
			}
		}
	});
	return addresses;
}
